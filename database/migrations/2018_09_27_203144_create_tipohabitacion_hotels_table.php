<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * ESTA TABLA CREA UNA RELACION ENTRE LOS TIPOS DE HABITACIONES
 * DE UNA CADENA HOTELERA Y SUS HOTELES, TOMA EN CUENTA
 * QIE NO NECESARIAMENTE UN HOTEL TIENE LOS MISMOS TIPOS DE HABITACIONES
 * QUE LOS OTROS DENTRO DE LA CADENA
 */
class CreateTipohabitacionHotelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipohabitacion_hotels', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('hotel_id')->unsigned();
            $table->integer('tipo_habitacion_id')->unsigned();


            $table->foreign('hotel_id')->references('id')->on('hotels');
            $table->foreign('tipo_habitacion_id')->references('id')->on('tipo_habitacions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipohabitacion_hotels');
    }
}
