<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEdificiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('edificios', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('nombre', 40)->nullable();
            $table->string('codigo', 20)->default('S/A')->index('CDOI');
            $table->integer('total_habitaciones')->default(1);
            $table->integer('hotel_id')->unsigned();
            $table->text('ubicacion')->nullable();
            $table->boolean('tiene_ascensor')->default(false);
            $table->boolean('tiene_zona_descanso')->default(true);
            $table->boolean('tiene_wifi')->default(true);
            $table->boolean('con_cabinas_internet')->default(false);

            // CANTIDAD DE PISOS DEL EDIFICIO + PLANTA BAJA
            $table->integer('cantidad_pisos')->default(1);

            $table->foreign('hotel_id')->references('id')->on('hotels');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('edificios');
    }
}
