<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacturaPagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factura_pagos', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->decimal('monto_pagado', 12, 2)->default(0.00);
            $table->decimal('porcentaje_equivalente', 3,2)->default(000.00);
            $table->integer('factura_id')->unsigned();
            $table->decimal('resto_pendiente', 12, 2)->default(0.00);
            $table->enum('tipo_pago', [
                'EFECTIVO',
                'CHEQUE',
                'CREDITO',
                'DEBITO'
            ])->default('DEBITO');

            $table->string('nro_referencia', 70)->nullable()->default('000')->index('REF');

            $table->foreign('factura_id')->references('id')->on('facturas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('factura_pagos');
    }
}
