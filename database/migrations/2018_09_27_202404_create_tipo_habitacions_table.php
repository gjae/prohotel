<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTipoHabitacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipo_habitacions', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('denominacion', 50);
            $table->text('descripcion')->nullable();
            $table->decimal('costo', 12,2)->default(0.00);
            $table->decimal('costo_preferencial_1', 12,2)->default(0.00);
            $table->decimal('costo_preferencial_2', 12,2)->default(0.00);

            // HACE REFERENCIA A LA CADENA HOTELERA
            // ASI UN TIPO DE HABITACION DE UN HOTEL 
            // PODRA SER IMPORTADO A N CANTIDAD DE HOTELES DE LA CADENA
            $table->integer('user_id')->unsigned();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipo_habitacions');
    }
}
