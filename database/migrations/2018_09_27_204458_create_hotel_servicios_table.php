<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


/**
 * ESTA TABLA AHORA SERA USADA PARA MANTENER CENTRALIZADAS
 * LAS RELACIONES ENTRE LOS SERVICIOS DISPONIBLES Y CADA PARTE DEL HOTEL
 * ES DECIR, LOS SERVICIOS CREADOS DISPONIBLES PARA EL HOTEL, LOS EDIFICIOS
 * Y LAS HABITACIONES
 */
class CreateHotelServiciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotel_servicios', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('hotel_id')->unsigned();
            $table->integer('servicio_id')->unsigned();
            $table->decimal('costo', 12,2)->default(0.00);
            $table->boolean('por_separado')->default(false);
            $table->integer('edificio_id')->insigned()->default(0);
            $table->integer('habitacion_id')->unsigned()->default(0);

            // REPRESENTA EL HORARIO DE TRABAJO DEL SERVICIO, EJEMPLO: SERVICIO
            // DE LAVANDERIA GENERALMENTE POSEE UN HORARIO DE TRABAJO

            $table->foreign('hotel_id')->references('id')->on('hotels');
            $table->foreign('servicio_id')->references('id')->on('servicios');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotel_servicios');
    }
}
