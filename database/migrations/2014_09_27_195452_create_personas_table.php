<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personas', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('nombres', 60);
            $table->string('apellidos', 60);
            $table->string('identificacion', 11)->index('DNI');
            $table->text('direccion')->nullable();
            $table->string('telefono', 24)->nullable();
            $table->string('correo_electronico', 72)->nullable();
            $table->text('origen')->nullable();
            $table->enum('tipo_identificacion', ['NIT', 'RUC'])->default('NIT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personas');
    }
}
