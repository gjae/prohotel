<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLimpiezasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('limpiezas', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('habitacion_id')->unsigned();
            $table->datetime('solicitud_at')->nullable();
            $table->datetime('limpiada_at')->nullable();
            $table->boolean('insidencia')->default(false);
            $table->text('descripcion_insidencia')->nullable();
            $table->boolean('cambio_sabanas')->nullable()->default(false);

            $table->integer('user_id')->unsigned();


            $table->foreign('habitacion_id')->references('id')->on('habitacions');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('limpiezas');
    }
}
