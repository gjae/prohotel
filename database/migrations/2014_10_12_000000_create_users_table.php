<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->softDeletes();
            $table->string('email')->unique("EM");
            $table->string('cadena')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
            $table->enum('rol', [
                'CDA_HOTEL', // CADENA HOTELERA, MAESTRO DE HOTELES - REGISTRA HOTELES
                'USER', // EMPLEADO COMUN
                'HUESPED', // HUESPED
                'ADMIN', // ADMINISTRADOR DEL SISTEMA
                'CONTADOR', // USUARIO DE CONTABILIDAD
                'CAJA', // USUARIO DE CAJA
            ])->default('USER');

            $table->integer('user_creador_id')->default(0)->unsigned();
            $table->integer('persona_id')->default(0)->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
