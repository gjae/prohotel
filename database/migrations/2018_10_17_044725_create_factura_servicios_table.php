<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacturaServiciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factura_servicios', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->datetime('fecha_consumo')->nullable();
            $table->datetime('fecha_registro')->nullable();
            $table->integer('servicio_id')->unsigned();
            $table->integer('factura_id')->unsigned();
            $table->decimal('costo', 12, 2)->default(0.00);
            $table->enum('estado', [
                'CONS', // SERVICIO  YA CONSUMIDO POR EL HUESPED
                'NOCONS', // SERVICIO AUN SIN CONSUMIR
            ])->default('NOCONS');

            $table->foreign('factura_id')->references('id')->on('facturas');
            $table->foreign('servicio_id')->references('id')->on('servicios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('factura_servicios');
    }
}
