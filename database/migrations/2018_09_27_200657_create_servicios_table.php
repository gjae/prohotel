<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servicios', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();

            $table->string('nombre', 22);
            $table->string('codigo', 22)->index('CDO');
            $table->integer('user_id')->unsigned();
            $table->text('descripcion')->nullable();
            
            $table->time('hora_inicio')->nullable();
            $table->time('hora_fin')->nullable();

            $table->enum('tipo_servicio', [ 
                'HOTEL' ,
                'EDIFICIO', 
                'HABITACION', 
                'PATIO', 
                'ZONA_VERDE', 
                'ESTACIONAMIENTO'
            ])->default('HOTEL');
            $table->decimal('costo', 12,2)->default(0.00);

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servicios');
    }
}
