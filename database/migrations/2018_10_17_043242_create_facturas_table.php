<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facturas', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->enum('tipo_factura', ['NIT', 'RUC'])->default('NIT');
            $table->string('identificacion', 22);
            $table->decimal('total_base', 12, 2)->default(0.00);
            $table->decimal('ivg', 12, 2)->default(0.00);
            $table->decimal('total', 12 ,2)->default(0.00);
            $table->integer('persona_id')->unsigned();
            $table->enum('facturado_por', [ 'D', 'M', 'A', 'HRS' ])->default('D');
            $table->text('motivo_visita')->nullable();
            $table->datetime('ingreso_at')->nullable();
            $table->datetime('salida_at')->nullable();
            $table->enum('estado_factura', ['AB', 'CE'])->default('AB');
            $table->enum('tiene_ivg', ['on', 'off', '1', '0'])->default('on');
            $table->decimal('subtotal', 12, 2)->default(0.00);
            $table->integer('habitacion_id')->unsigned();

            $table->foreign('persona_id')->references('id')->on('personas');
            $table->foreign('habitacion_id')->references('id')->on('habitacions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facturas');
    }
}
