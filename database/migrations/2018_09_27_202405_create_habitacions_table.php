<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHabitacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('habitacions', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('codigo', 10)->default('01')->index('CDII');
            $table->integer('hotel_id')->unsigned();
            $table->integer('tipo_habitacion_id')->unsigned();
            $table->boolean('ocupada')->default(false);
            $table->integer('edificio_id')->unsigned();

            // UNICACION DE LA HABITACION DENTRO DEL EDIFICIO
            $table->string('piso_edificio', 7)->default('01');
            $table->foreign('edificio_id')->references('id')->on('edificios');
            $table->foreign('hotel_id')->references('id')->on('hotels');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('habitacions');
    }
}
