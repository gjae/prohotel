<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
        	'email' => 'admin@admin.com',
        	'password' => '123456',
        	'cadena' => 'Prueba Resorts',
        	'rol' => 'CDA_HOTEL'
        ]);
    }
}
