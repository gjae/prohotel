<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Servicio extends Model
{
    use SoftDeletes;
    protected $table = 'servicios';
    protected $fillable = [
    	'nombre', 'codigo', 'user_id', 'descripcion', 'costo', 'tipo_servicio', 'hora_inicio',
        'hora_fin'
    ];

    protected $casts = [
        'hora_fin' => 'time',
        'hora_inicio' => 'time'
    ];


    public function hoteles(){
    	return $this->hasMany('App\Models\HotelServicio');
    }

    public function creado_por(){
    	return $this->belongsTo('App\User');
    }

    public function getHoraInicioAttribute($old){
        return new Carbon($old);
    }
    public function getHoraFinAttribute($old){
        return new Carbon($old);
    }

    public function facturas(){
        return $this->hasMany('App\Models\FacturaServicio');
    }
}
