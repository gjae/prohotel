<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CaracteristicaHabitacion extends Model
{
    use SoftDeletes;

    protected $table = 'caracteristica_habitacions';
    protected $fillable = [
    	'habitacion_id', 'caracteristica_id'
    ];


    public function caracteristica(){
    	return $this->belongsTo('App\Models\Caracteristica');
    }	

    public function habitacion(){
    	return $this->belongsTo('App\Models\Habitacion');
    }
}
