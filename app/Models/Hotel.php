<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Hotel extends Model
{
    use SoftDeletes;
    protected $table = 'hotels';
    protected $fillable = [
    	'user_id', 'nombre', 'direccion', 'total_edificios', 'total_habitaciones', 'estacionamiento'
    ];


    public function edificios(){
    	return $this->hasMany('App\Models\Edificio');
    }

    public function habitaciones(){
    	return $this->hasMany('App\Models\Habitacion');
    }

    public function cadena(){
        return $this->belongsTo('App\User', 'user_id');
    }

    public function servicios(){
        return $this->hasMany('App\Models\HotelServicio');
    }

    public function empleados(){
        return $this->hasMany('App\Models\HotelUser');
    }

    public function tipos_habitaciones(){
        return $this->hasMany('App\Models\TipohabitacionHotel');
    }
}
