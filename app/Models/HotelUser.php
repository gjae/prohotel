<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HotelUser extends Model
{
    use SoftDeletes;
    protected $table = 'hotel_users';
    protected $fillable = [
    	'user_id', 'hotel_id'
    ];


    public function hotel(){
    	return $this->belongsTo('App\Models\Hotel');
    }

    public function empleado(){
    	return $this->belongsTo('App\User');
    }
}
