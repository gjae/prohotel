<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Persona extends Model
{
    use SoftDeletes;
    protected $table = 'personas';
    public $fillable = [
    	'nombres',
    	'apellidos',
    	'identificacion',
    	'direccion',
    	'telefono',
    	'correo_electronico',
    	'origen',
    	'tipo_identificacion'
    ];

    public function user(){
        return $this->hasOne('App\User');
    }

    public function facturas(){
        return $this->hasMany('App\Models\Factura');
    }
}
