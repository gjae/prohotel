<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CaracteristicaEdificio extends Model
{
    use SoftDeletes;
    protected $table = "caracteristica_edificios";
    protected $fillable = [
    	'edificio_id', 'caracteristica_id'
    ];


}
