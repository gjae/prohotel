<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Edificio extends Model
{
    use SoftDeletes;
    protected $table = 'edificios';
    protected $fillable = [
    	'nombre', 
        'codigo', 
        'total_habitaciones', 
        'hotel_id', 
        'cantidad_pisos',
        'ubicacion',
        'tiene_ascensor',
        'tiene_zona_descanso',
        'tiene_wifi',   
        'con_cabinas_internet'
    ];

    public function hotel(){
    	return $this->belongTo('App\Models\Hotel');
    }

    public function habitaciones(){
    	return $this->hasMany('App\Models\Habitacion');
    }

    public function servicios(){
        return $this->hasMany('App\Models\HotelServicio');
    }
}
