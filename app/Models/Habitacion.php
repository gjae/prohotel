<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Habitacion extends Model
{
    use SoftDeletes;
    protected $table = 'habitacions';
    protected $fillable = [
    	'codigo', 'hotel_id', 'tipo_habitacion_id', 'piso_edificio', 'edificio_id'
    ];


    public function caracteristicas(){
    	return $this->hasMany('App\Models\CaracteristicaHabitacion');
    }

    public function hotel(){
    	return $this->belongsTo('App\Models\Hotel');
    }

    public function tipo(){
    	return $this->belongsTo('App\Models\TipoHabitacion', 'tipo_habitacion_id');
    }

    public function edificio(){
        return $this->belongsTo('App\Models\Edificio');
    }

    public function facturas(){
        return $this->hasMany('App\Models\Factura');
    }

    public function reserva_activa(){
        return $this->hasOne('App\Models\Factura')->whereEstadoFactura('AB');
    }

    public function limpiezas(){
        return $this->hasMany('App\Models\Limpieza');
    }

    public function limpieza(){
        return $this->hasOne('App\Models\Limpieza')->whereNull('limpiada_at');
    }

}
