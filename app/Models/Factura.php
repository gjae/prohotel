<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon\Carbon;
class Factura extends Model
{
	use SoftDeletes;
	protected $table = 'facturas';
	protected $fillable = [
		'tipo_identificacion_factura',
		'identificacion_factura',
		'total_base',
		'ivg',
		'total',
		'persona_id',
		'facturado_por',
		'motivo_visita',
		'ingreso_at',
		'salida_at',
		'estado_factura',
		'tiene_ivg',
		'subtotal',
		'habitacion_id'
	];

	protected $casts = [
		'ingreso_at' => 'datetime',
		'salida_at' => 'datetime'
	];
	protected $dates = [ 'deleted_at', 'ingreso_at', 'salida_at' ];


	public function persona(){
		return $this->belongsTo('App\Models\Persona');
	}

	public function habitacion(){
		return $this->belongsTo('App\Models\Habitacion');
	}

	public function servicios(){
		return $this->hasMany('App\Models\FacturaServicio');
	}


	public function pagos(){
		return $this->hasMany('App\Models\FacturaPago');
	}

	public static function fillables(){
		return  [
			'tipo_identificacion_factura',
			'identificacion_factura',
			'total_base',
			'ivg',
			'total',
			'persona_id',
			'facturado_por',
			'motivo_visita',
			'ingreso_at',
			'salida_at',
			'estado_factura',
			'tiene_ivg',
			'subtotal',
			'habitacion_id'
		];
	}
  
}
