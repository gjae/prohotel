<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Limpieza extends Model
{
    use SoftDeletes;
    protected $table = 'limpiezas';
    protected $fillable = [
    	'habitacion_id',
		'solicitud_at',
		'limpiada_at',
		'insidencia',
		'descripcion_insidencia',
		'cambio_sabanas',
		'user_id'
    ];

    protected $casts = [
    	'solicitud_at' => 'datetime',
    	'limpiada_at' => 'datetime',
    ];

    protected $dates = ['deleted_at', 'solicitud_at', 'limpiada_at'];

    public function habitacion(){
    	return $this->belongsTo('App\Models\Habitacion');
    }

    public function solicitante(){
    	return $this->belongsTo('App\User');
    }
}
