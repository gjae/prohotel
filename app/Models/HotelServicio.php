<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HotelServicio extends Model
{
    use SoftDeletes;
    protected $table = 'hotel_servicios';
    protected $fillable = [
    	'hotel_id', 'servicio_id', 'costo', 'por_separado', 'hora_inicio', 'hora_fin', 'edificio_id', 'habitacion_id'
    ];

    public function hotel(){
    	return $this->belongsTo('App\Models\Hotel');
    }

    public function servicio(){
    	return $this->belongsTo('App\Models\Servicio');
    }
}
