<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FacturaServicio extends Model
{
    use SoftDeletes;
    protected $table = 'factura_servicios';
    protected $fillable = [
    	'factura_id', 'servicio_id', 'costo', 'fecha_consumo', 'fecha_registro', 'estado'
    ];


    public function factura(){
    	return $this->belongsTo('App\Models\Factura');
    }

    public function servicio(){
    	return $this->belongsTo('App\Models\Servicio');
    }
}
