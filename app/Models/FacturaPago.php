<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FacturaPago extends Model
{
    use SoftDeletes;
    protected $table = 'factura_pagos';
    protected $fillable = [
		'monto_pagado',
		'porcentaje_equivalente',
		'factura_id',
		'resto_pendiente',
		'tipo_pago',
		'nro_referencia',
    ];


    public function factura(){
    	return $this->belongsTo( 'App\Models\Factura' );
    }
}
