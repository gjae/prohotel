<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipohabitacionHotel extends Model
{
    use SoftDeletes;
    protected $table = 'tipohabitacion_hotels';
    protected $fillable = [
    	'hotel_id', 'tipo_habitacion_id'
    ];

    public function hotel(){
    	return $this->belongsTo('App\Models\Hotel');
    }

    public function tipo(){
    	return $this->belongsTo('App\Models\TipoHabitacion');
    }
}
