<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Caracteristica extends Model
{
    use SoftDeletes;
    protected $table = 'caracteristicas';
    protected $fillable = [
    	'denominacion', 'descripcion', 'costo'
    ];


    public function habitaciones(){
    	return $this->hasMany('App\Models\CaracteristicaHabitacion');
    }
}
