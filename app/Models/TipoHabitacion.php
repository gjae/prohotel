<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoHabitacion extends Model
{
    use SoftDeletes;
    protected $table = 'tipo_habitacions';
    protected $fillable = [
    	'denominacion', 'descripcion', 'costo', 'user_id', 'costo_preferencial_1','costo_preferencial_2'
    ];


    public function habitaciones(){
    	return $this->hasMany('App\Models\Habitacion');
    }

    public function cadena(){
    	return $this->belongsTo('App\User');
    }

    public function hoteles(){
    	return $this->hasMany('App\Models\TipohabitacionHotel');
    }

}
