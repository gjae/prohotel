<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Persona;
use App\Models\Hotel;
use App\Models\Factura;
use App\Models\FacturaServicio as FS;
use App\Models\Servicio;

use DB;
use Carbon\Carbon;
use Auth;

class GestionDeServicios extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($hotel_id, Request $request) 
    {
        $factura = Factura::find($request->factura);
        $data = [
            'error' => false,
            'modal_title' => 'Agregar servicios a factura',
            'html' => \View::make('pages.partials.servicio', [ 
                'servicio' => false, 
                'factura' => $factura ,
                'hotel' => $hotel_id,
                'user' => Auth::user(),
                'persona' => !is_null( Auth::user()->persona )
            ])->render()
        ];

        return response($data, 200)->header('Content-Type', 'application/json');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($hotel_id ,Request $request)
    {

        DB::beginTransaction();
        try {
            foreach ($request->servicios as $key => $servicio) {
               $fs = new FS;
               $fs->factura_id = $request->factura_id;
               $fs->fecha_registro = Carbon::now()->format('Y-m-d H:i:s');
               $fs->servicio_id = $servicio;
               $fs->costo = Servicio::find($servicio)->costo;

               $fs->save();
            }

            $factura = Factura::find( $request->factura_id );
            $factura->total += $request->total;
            $factura->ivg += $request->total_ivg;
            $factura->save();
            
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->to( url( "hotel/$hotel_id/gestion/servicios" ) )->with('error', 'Error: '.$e->getMessage());
        }

        return redirect()->to( url( "hotel/$hotel_id/gestion/servicios/$request->factura_id" ) )->with('success', 'Los datos han sido guardados correctamente');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($hotel_id, $id)
    {
        $data = [
            'factura' => Factura::find($id),
            'hotel' => Hotel::find($hotel_id),
            'servicios' => FS::whereFacturaId($id)->get(),
            'user' => Auth::user(),
            'persona' => !is_null( Auth::user()->persona ),
            'set_hotel' => false
        ];

        return view('pages.gestion_servicios', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($hotel_id, $id, Request $request)
    {
        $serv = FS::find($id);
        switch ( $request->type ) {
            case 'CONSUMIDO':
                $serv->estado = 'CONS';
            break;
            case 'DELETE' :
                $serv->deleted_at = Carbon::now()->format('Y-m-d H:i:s');
            break;
        }

        if( $serv->save() ) 
            return response(['error' => false,'message' => 'Proceso realizado correctamente'], 200)
            ->header('Content-Type', 'application/json');

        return response(['error' => true, 'message' => 'Error al intenger procesar el registro'], 200)
        ->header('Content-Type', 'application/json');
    }
}
