<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Hotel;
use App\Models\Edificio;
use App\Models\Habitacion;

use App\User;
use Auth;

class HotelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hoteles = Hotel::whereUserId(Auth::user()->id)->get();
        $users = User::whereUserCreadorId( Auth::user()->id )
        ->where('user_creador_id', '<>', 0)->where('persona_id', '<>', 0)->get();
        
        return view('generics.set_hotel', ['hoteles' => $hoteles, 'set_hotel' => true, 'users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if( Auth::user()->rol != 'CDA_HOTEL' && !$request->ajax() ) return redirect()->to( url('hotel') );
        else if( $request->ajax() ){
            return response([
                'is_ajax' => true,
                'error' => false,
                'html' => \View::make('partials.register_hotel')->render(),
                'modal_title' => 'Administracion de hoteles'
            ], 202)->header('Content-Type', 'application/json');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if( Auth::user()->rol == 'CDA_HOTEL' ){
            Hotel::create( $request->except(['_token']) );
            return redirect()->to( url('hotel') );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($hotel_id)
    {

        $edificios = Edificio::whereHotelId($hotel_id)->get(['id']);
        $edificios_ids = [];
        foreach ($edificios as $key => $id) {
            $edificios_ids[] = $id["id"];
        }



        $habitaciones = Habitacion::whereIn('edificio_id', $edificios_ids)->get();
        
        $hotel = Hotel::find($hotel_id);
        $user = Auth::user();
        $persona = !is_null($user->persona);

        return view('pages.gestion_habitacion', [
            'persona' => $persona,
            'hotel' => $hotel,
            'user' => $user,
            'habitaciones' => $habitaciones,
            'set_hotel' => false
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($hotel_id, Request $request)
    {
        $user = User::whereEmail( Auth::user()->email )->first();

       if( $user ){
            if( \Hash::check( $request->password, $user->password ) ){
                $hotel = Hotel::find($hotel_id);
                $hotel->delete();

                return redirect()
                ->to( url("hotel") );
            }

            return redirect()
            ->to( url( "hotel/$hotel_id/eliminar" ) );
       }
       return dd( $hotel_id );
    }

    public function config($hotel_id)
    {  
        if( Auth::user()->rol != 'CDA_HOTEL' ) 
            return redirect()->to( url("/hotel/$hotel_id") );

        return view('pages.hotel_config', [
            'set_hotel' => false,
            'hotel' => Hotel::find($hotel_id),
            'user' => Auth::user(),
            'persona' => !is_null(Auth::user()->persona)
        ]);
    }
}
