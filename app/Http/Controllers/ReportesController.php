<?php

namespace App\Http\Controllers;
use App\Models\Hotel;
use App\Models\Edificio;

use Illuminate\Http\Request;

class ReportesController extends Controller
{
    public function reportes($hotel_id, Request $request){
    	$hotel = Hotel::find( $hotel_id );
    	$edificio = Edificio::whereHotelId($hotel_id);
    	if(  $request->edificio != "TODO" ){
    		$edificio = $edificio->whereId($request->edificio);
    	}

    	return dd($edificio->get());


    	return view( "reportes.".strtolower($request->tipo), ["hotel" => $hotel, "edificio" => $edificio->get() ] );
    }
}
