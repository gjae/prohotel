<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Hotel;
use App\Models\Factura;
use App\Models\Persona;
use App\Models\FacturaPago as FP;

use Auth;

class GestionDePagos extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($hotel_id, Request $request)
    {
        $data = [
            'error' => false,
            'html' => \View::make('pages.partials.pago', [
                'factura' => Factura::find($request->factura) , 
                'edit' => false,
                'hotel' => Hotel::find($hotel_id)
            ])->render(),
            'modal_title' =>  'Registro de pago',
            'factura' => false
        ];

        return response($data, 200)->header('Content-Type', 'application/json');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($hotel_id, Request $request)
    {
        $factura = FP::create($request->all());

        return redirect()->to( url("hotel/$hotel_id/gestion/pagos/$request->factura_id") );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($hotel_id, $id)
    {
        $data = [
            'factura' => Factura::find($id),
            'user' => Auth::user(),
            'persona' => !is_null( Auth::user()->persona ),
            'hotel' => Hotel::find($hotel_id),
            'set_hotel' => false,
            'factura_id' => $id,
            'url' => "hotel/$hotel_id/gestion/pagos/create?factura=$id"
        ];

        return view('pages.gestion_pagos', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
