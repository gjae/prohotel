<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\Hotel;
use App\Models\TipoHabitacion;

class TiposHabitacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($hotel_id)
    {
        $hotel = Hotel::find($hotel_id);
        $user = Auth::user();
        $persona = !is_null($user->persona);



        return view('generics.tipos_habitacion', [
            'set_hotel' => false,
            'persona' => $persona,
            'user' => $user,
            'hotel' => $hotel,
            'url' => "hotel/$hotel_id/habitaciones/tipos"
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($hotel_id)
    {
        $hotel = Hotel::find($hotel_id);
        $data = [
            'error' => false,
            'html' => \View::make('partials.tipo_habitacion', ['hotel' => $hotel, 'tipo_habitacion' => false, 'url' => "hotel/$hotel_id/habitaciones/tipos"])->render(),
            'modal_title' => 'Tipo de habitación',
            'url' => "hotel/$hotel_id/habitaciones/tipos"
        ];

        return response($data, 200)->header('Content-Type', 'application/json');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($hotel_id, Request $request)
    {
        $th = new TipoHabitacion($request->all());
        $th->user_id = Auth::user()->id;
        $th->save();
        return redirect()
        ->to( url("hotel/$hotel_id/habitaciones/tipos") );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($hotel_id, $id)
    {

        $hotel = Hotel::find($hotel_id);
        $data = [
            'error' => false,
            'html' => \View::make('partials.tipo_habitacion', [
                'tipo_habitacion' => TipoHabitacion::find($id),
                'hotel' => Hotel::find($hotel_id),
                'url' => "hotel/$hotel_id/habitaciones/tipos/$id"
            ])->render(),
            'modal_title' => 'Tipo de habitación',
            
        ];

        return response($data, 200)->header('Content-Type', 'application/json');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($hotel_id, Request $request, $id)
    {
        $th = TipoHabitacion::find($id);
        $th->fill( $request->except(['_method', '_token']) );

        $th->save();

        return redirect()
        ->to( url( "hotel/$hotel_id/habitaciones/tipos" ) )
        ->with('success', 'Actualizacion realizada correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($hotel_id, $id)
    {
        $th = TipoHabitacion::find($id);
        if( !is_null($th) )
            $th->delete();

        $data = [
            'error' => false,
            'message' => 'El registro fue eliminado correctamente'
        ];

        return response($data, 200)->header('Content-Type', 'application/json');
    }
}
