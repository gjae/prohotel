<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Caracteristica;
use App\Models\Hotel;
use App\Models\Edificio;

use Auth;
class CaracteristicaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($hotel_id)
    {   

        return view('generics.caracteristicas', [
            'set_hotel' => false,
            'hotel' => Hotel::find($hotel_id),
            'user' => Auth::user(),
            'persona' => !is_null( Auth::user()->persona ),
            'caracteristica' => false
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($hotel_id)
    {
        return response([
            'error' => false,
            'html' => \View::make('partials.caracteristica', [
                'hotel' => Hotel::find($hotel_id),
                'caracteristica' => false,
                'user' => Auth::user(),
                'persona' => !is_null( Auth::user()->persona ),
                'url' => "hotel/$hotel_id/edificios/caracteristicas"
            ])->render(),
            'modal_title' => 'Configuracion de caracteristicas'
        ], 200)->header('Content-Type', 'application/json');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($hotel_id , Request $request)
    {
        $c = new Caracteristica($request->all());

        $c->user_id = Auth::user()->id;
        $c->save();
        return redirect()
        ->to( url("hotel/$hotel_id/edificios/caracteristicas") );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($hotel_id , $id)
    {
        return response([
            'error' => false,
            'html' => \View::make('partials.caracteristica', [
                'hotel' => Hotel::find($hotel_id),
                'caracteristica' => Caracteristica::find($id),
                'user' => Auth::user(),
                'persona' => !is_null( Auth::user()->persona ),
                'url' => "hotel/$hotel_id/edificios/caracteristicas/$id"
            ])->render(),
            'modal_title' => 'Configuracion de caracteristicas'
        ], 200)->header('Content-Type', 'application/json');        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($hotel_id , Request $request, $id)
    {
        $c = Caracteristica::find($id);
        $c->fill( $request->except(['_token', '_method']) );
        $c->save();


        return redirect()
        ->to( url("hotel/$hotel_id/edificios/caracteristicas") );        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($hotel_id, $id)
    {
        $c = Caracteristica::find($id);
        if( $c )
            $c->delete();

        return response([
            'error' => false,
            'message' => 'La caracteristica ha sido elimada correctamente'
        ], 200)->header('Content-Type', 'application/json');
    }
}
