<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\Hotel;
use App\Models\Habitacion;

use App\Models\Edificio;

use App\Models\HotelServicio;

class HabitacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($hotel_id)
    {
        $hotel = Hotel::find($hotel_id);
        $user = Auth::user();
        $persona = !is_null($user->persona);



        return view('generics.habitaciones', [
            'set_hotel' => false,
            'persona' => $persona,
            'user' => $user,
            'hotel' => $hotel
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($hotel_id)
    {
        $hotel = Hotel::find($hotel_id);
        $data = [
            'error' => false,
            'html' => \View::make('partials.habitacion', [
                'hotel' => $hotel,  
                'url' => "hotel/$hotel_id/habitaciones", 
                'habitacion' => false
            ])->render(),
            'modal_title' => 'Tipo de habitación',
            'url' => "hotel/$hotel_id/habitaciones"
        ];

        return response($data, 200)->header('Content-Type', 'application/json');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($hotel_id, Request $request)
    {

        $servicios = $request->servicios;

       $dataRequest = $request->all();
        $dataRequest['edificio_id'] = explode('-', $dataRequest['edificio_id'])[0];
       $dataRequest["hotel_id"] = $hotel_id;
       $habitacion = Habitacion::create($dataRequest);

        $this->saveServicios( $hotel_id, $habitacion->id, $servicios );
       $edificio = Edificio::find($dataRequest['edificio_id']);

       $edificio->total_habitaciones += 1;
       $edificio->save();
       return redirect()
       ->to( url( "hotel/$hotel_id/habitaciones" ) );

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($hotel_id, $id)
    {
        $hotel = Hotel::find($hotel_id);
        $data = [
            'error' => false,
            'html' => \View::make('partials.habitacion', [
                'hotel' => $hotel,  
                'url' => "hotel/$hotel_id/habitaciones/$id", 
                'habitacion' => Habitacion::find($id)
            ])->render(),
            'modal_title' => 'Tipo de habitación',
            'url' => "hotel/$hotel_id/habitaciones"
        ];

        return response($data, 200)->header('Content-Type', 'application/json');
    }

    private function saveServicios($hotel_id, $id, $servicios){
        if ( $servicios == null ) return false;
        foreach ($servicios as $key => $servicio_id) {
            HotelServicio::create([
                'servicio_id' => $servicio_id,
                'habitacion_id' => $id,
                'hotel_id' => $hotel_id
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($hotel_id, Request $request, $id)
    {
        $habitacion = Habitacion::find($id);

        $dataRequest = $request->all();       


        $dataRequest['edificio_id'] = explode("-", $dataRequest['edificio_id'])[0];

        $habitacion->fill( $dataRequest);
        $habitacion->save();

        $servicios = $request->servicios;

        $hs = HotelServicio::whereHabitacionId($id)->get();

        if( $hs->isEmpty() && !is_null($servicios) ){
            $this->saveServicios($hotel_id, $id, $servicios);
        }else if( !$hs->isEmpty() ) {

            HotelServicio::whereHabitacionId($id)->forceDelete();

            if(  !is_null($servicios) )
                $this->saveServicios($hotel_id, $id, $servicios);
        }



        return redirect()
        ->to( url("hotel/$hotel_id/habitaciones") );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($hotel_id, $id)
    {
        $habitacion = Habitacion::find($id);
        $edificio = Edificio::find($habitacion->edificio_id);
        $edificio->total_habitaciones -= 1;
        $edificio->save();
        if( !is_null( $habitacion ) )
            $habitacion->delete();

        return response([
            'error' => false,
            'message' => 'Registro eliminado de manera correcta'
        ], 200)->header('Content-Type', 'application/json');
    }
}
