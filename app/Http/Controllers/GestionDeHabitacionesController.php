<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Habitacion;
use App\Models\Hotel;
use App\Models\Edificio;
use App\Models\Factura;
use App\Models\Limpieza;

use Carbon\Carbon;
use Auth;
use DB;

class GestionDeHabitacionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($hotel_id)
    {
        $edificios = Edificio::whereHotelId($hotel_id)->get(['id']);
        $edificios_ids = [];
        foreach ($edificios as $key => $id) {
            $edificios_ids[] = $id["id"];
        }



        $habitaciones = Habitacion::whereIn('edificio_id', $edificios_ids)->get();
        
        $hotel = Hotel::find($hotel_id);
        $user = Auth::user();
        $persona = !is_null($user->persona);

        return view('pages.gestion_habitacion', [
            'persona' => $persona,
            'hotel' => $hotel,
            'user' => $user,
            'habitaciones' => $habitaciones,
            'set_hotel' => false
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($hotel_id, $id)
    {
        $hotel = Hotel::find($hotel_id);
        $habitacion = Habitacion::find($id);
        $factura = Factura::whereHabitacionId( $id )->whereEstadoFactura('AB')->first();

        $data = [
            'error' => false,
            'html' => \View::make('pages.partials.habitacion', [
                'hotel' => $hotel,
                'habitacion' => $habitacion,
                'factura' => $factura
            ])->render(),
            'modal_title' => 'Detalle de la habitación'
        ];


        return response($data, 200)->header('Content-Type', 'application/json');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function solicitar_limpieza($hotel_id, $habitacion_id){

        $habitacion = Habitacion::find($habitacion_id);


        $hotel = Hotel::find($hotel_id);
        $data = [
            'error' => false,
            'modal_title' => 'Solicitud de limpieza',
            'html' => \View('pages.partials.limpieza', [
                'habitacion' => $habitacion,
                'hotel' => $hotel,
            ])->render()
        ];

        return response($data , 200)->header('Content-Type', 'application/json');
    }

    public function guardar_solicitud($hotel_id, $habitacion_id, Request $request){

        $habitacion = Habitacion::find($habitacion_id);
        if( !is_null($habitacion->limpieza) ) 
            return redirect()->to(  url("hotel/$hotel_id/gestion/habitaciones") )
                ->with("error", 'Ya existe una solicitud de limpieza sin atender');

        $limpieza = Limpieza::create($request->all());
        return redirect()->to( url("hotel/$hotel_id/gestion/habitaciones") )->with("success", 'Se ha guardado la solicitud de limpieza para la habitacion');
    }

    public function limpiada($hotel_id, $habitacion_id){
        $limpiada = Limpieza::whereHabitacionId($habitacion_id)->whereNull("limpiada_at")->first();
        if( $limpiada ){
            $limpiada->limpiada_at = Carbon::now()->format('Y-m-d H:i:s');
        }
        $limpiada->save();

        return response(['error' =>false ,'message' => 'Se ha actualizado el estado de la solicitud de limpieza'], 200)
        ->header('Content-Type', 'application/json');

    }

    public function tiposDeReporte($hotel_id){

        $hotel = Hotel::find($hotel_id);
        $data = [
            'error' => false,
            'html' => \View::make('pages.partials.tipos_reportes', ['hotel' => $hotel])->render(),
            'modal_title' => 'Reportes de habitaciones'
        ];

        return response($data, 200)->header("Content-Type", 'application/json');

    }

    public function cerrarFactura($hotel_id, $factura_id){
        DB::beginTransaction();
        $data = [];
        try{

            $factura = Factura::find( $factura_id );

            if( $factura->total - $factura->pagos->sum('monto_pagado') == 0 ){
                $habitacion = $factura->habitacion;
                $factura->estado_factura = "CE";
                $factura->save();
                $limpieza = Limpieza::create([
                    'habitacion_id' => $habitacion->id,
                    'solicitud_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'descripcion_insidencia' => 'Retirada de huesped del hotel',
                    'insidencia' => 1,
                    'cambio_sabanas' => 1,
                    'user_id' => Auth::user()->id
                    
                ]);

                $habitacion->ocupada = 0;
                $habitacion->save();
            }
            else throw new \Exception("Esta factura no ha sido completamente pagada, por lo tanto, no puede ser cerrada");


            DB::commit();

            $data["message"] = "El cierre de la factura se ha realizado correctamente";
            $data["error"] = false;
        }catch( \Exception $e){
            DB::rollback();
            $data["message"] = "Error al intentar realizar el cierre de la factura: ".$e->getMessage();
            $data["error"] = true;
        }

        return response($data, 200)->header('Content-Type', 'application/json');
    }
}
