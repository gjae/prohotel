<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Persona;
use App\Models\HotelUser;
use Auth;
use DB;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if( Auth::user()->rol == 'CDA_HOTEL'  && $request->ajax() ){
            return response([
                'error' => false,
                'is_ajax' => true,
                'html' => \View::make('partials.register_user', ['user' => false, 'persona' => false])->render(),
                'modal_title' => 'Control de usuarios'
            ], 200)->header('Content-Type', 'application/json');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if( Auth::user()->rol == 'CDA_HOTEL' ){
            if( !$request->ajax() ){
                DB::beginTransaction();
                try {
                    $persona = Persona::create($request->all());
                    $persona->user()->save(
                        new User($request->all())
                    );

                    $persona->user->hotel()->save(
                        new HotelUser($request->all())
                    );
                    DB::commit();
                    return redirect()->to( url('hotel') );
                } catch (\Exception $e) {
                    DB::rollback();
                    return dd( $e->getMessage() );
                }
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return dd($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //parse_str(str)
       $user = User::find($id);
       $data = [
        'error' => false,
        'html' => \View::make('partials.register_user', ['user' => $user, 'persona' => $user->persona])->render()
       ];

       return response($data, 200)->header('Content-Type', 'application/json');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $persona = $request->only([
            'nombres', 
            'apellidos', 
            'tipo_identificacion',
            'identificacion',
            'telefono',
            'direccion',
        ]);

        return dd( $persona );
        $user = User::find($id);

        $persona_db = Persona::find($user->persona_id);

        foreach ($persona as $clave => $valor) {
            $persona_db->$clave = $valor;
        }
        $persona_db->save();

        if( $user->puesto !== null && ( $user->puesto->hotel_id != $request->hote_id ))
        {
            $hu = HotelUser::find($user->puesto->id);
            if( $request->hote_id == 'QUITAR' ) $hu->delete();
            else $hu->hotel_id = $request->hotel_id;

            $hu->save();

        }
        if( $request->hotel_id != 'QUITAR' && $user->puesto == null ){
            HotelUser::create(['user_id' => $id, 'hotel_id' =>$request->hotel_id]);
            return dd( "--");
        }

        $user->rol = $request->rol;
        $user->save();

        return  redirect()->to( url('hotel') );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        return dd($user);
    }
}
