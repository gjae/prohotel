<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Servicio;
use App\Models\Hotel;
use Auth;
class ServiciosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($hotel_id)
    {
        $user = Auth::user();

        $servicios = Servicio::whereUserId($user->id)->get();
        $persona = !is_null($user->persona);

        return view('generics.servicios', [
            'servicios' => $servicios,
            'user' => $user,
            'persona' => $persona,
            'set_hotel' => false,
            'hotel' => Hotel::find($hotel_id)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($hotel_id)
    {
        $hotel = Hotel::find($hotel_id);
        $user = Auth::user();
        $url = "hotel/$hotel_id/servicios";

        $data = [
            'error' => false,
            'html' => \View::make('partials.servicio',[
                'hotel' => $hotel,
                'user' => $user,
                'servicio' => false ,
                'url' => $url
            ])->render()
        ];

        return response($data, 200)->header('Content-Type', 'application/json');
    }   

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($hotel_id, Request $request)
    {
        $dataRequest = $request->all();

        $servicio = new Servicio($request->except(['_token']));
        $servicio->user_id = Auth::user()->id;

        $servicio->save();

        return redirect()->to( url( "hotel/$hotel_id/servicios" ) );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($hotel_id, $id)
    {
        $data = [
            'hotel' => Hotel::find($hotel_id),
            'servicio' => Servicio::find($id),
            'user' => Auth::user()
        ];
        $data['persona'] = !is_null( $data['user']->persona );

        $data['url'] = "hotel/$hotel_id/servicios/$id";

        $respuesta = [
            'error' => false,
            'html' => \View::make('partials.servicio', $data)->render()
        ];


        return response( $respuesta, 200)->header('Content-Type', 'application/json');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($hotel_id , Request $request, $id)
    {
        $servicio = Servicio::find( $id );

        foreach ($request->except(['_token', '_method']) as $clave => $valor) {
            $servicio->$clave = $valor;
        }

        $servicio->save();

        return redirect()->to( url( "hotel/$hotel_id/servicios" ) );

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($hotel_id, $id)
    {
        $servicio = Servicio::whereId($id)->whereUserId( Auth::user()->id )->first();

        if( $servicio )
            $servicio->delete();

        return response([
            'error' => false,
            'message' => 'El servicio ha sido eliminado correctamente'
        ], 200)->header('Content-Type', 'application/json');
    }


}
