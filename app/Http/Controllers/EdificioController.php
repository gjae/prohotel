<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Edificio;
use App\Models\Hotel;
use App\Models\HotelServicio;
use App\Models\CaracteristicaEdificio as CE;
use Auth;

class EdificioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($hotel_id)
    {
        $edificios = Edificio::whereHotelId($hotel_id)->get();
        $hotel = Hotel::find($hotel_id);
        $user = Auth::user();
        return view('generics.edificios', [
            'hotel' => $hotel,
            'edificios' => $edificios, 
            'persona' => !is_null( $user->persona), 
            'user' => $user  ,
            'set_hotel' => false
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($hotel_id)
    {
        $hotel = Hotel::find($hotel_id);
        $user = Auth::user();
        $persona = !is_null($user->persona);
        $edificio = false;
        $url = "hotel/$hotel_id/edificios";
        $data = [
            'error' => false,
            'html' => \View::make('partials.edificio', compact('hotel', 'user', 'persona', 'edificio', 'url'))->render(),
            'modal_title' => 'Creación de edificio'
        ];

        return response($data, 200)->header('Content-Type', 'application/json');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($hotel_id , Request $request)
    {

        $servicios = $request->servicios;
        $caracteristicas = $request->caracteristicas;

        $edificio = new Edificio($request->except(['_token', 'caracteristicas']));

        $edificio->hotel_id = $hotel_id;
        $edificio->save();

        $this->saveCaracteristicas($hotel_id, $edificio, $caracteristicas);
        return redirect()->to( url("hotel/$hotel_id/edificios") )->with('success', 'El edificio fue agregado correctamente
            ');
    }


    private function saveCaracteristicas($hotel_id, $edificio, $caracteristicas){

        if( isset($caracteristicas) && !is_null($caracteristicas) && is_array($caracteristicas) && count($caracteristicas) ){
            foreach ($caracteristicas as $key => $caracteristca) {
                CE::create([
                    'edificio_id' => $edificio->id,
                    'caracteristica_id' => $caracteristca
                ]);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($hotel_id, $id)
    {
        $hotel = Hotel::find($hotel_id);
        $edificio = Edificio::find($id);
        $user = Auth::user();
        $persona = !is_null($user->persona);
        $url = '';

        $url = "hotel/$hotel->id/edificios/$id";

        $data = [
            'error' => false,
            'html' => \View::make('partials.edificio', [
                'hotel' => $hotel,
                'edificio' => $edificio,
                'user' => $user,
                'persona' => $persona,
                'url' => $url
            ])->render(),
            'modal_title' => "Edición de edificios"
        ];  


        return response( $data , 200)->header('Content-Type', 'application/json');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($hotel_id, Request $request, $id)
    {

        $cars = $request->caracteristicas;

       $edificio = Edificio::find($id);
       $dataRequest = $request->except(['_method', '_token', 'caracteristicas']);
        CE::whereEdificioId($id)->whereIn('caracteristica_id', $cars)->forceDelete();

        $this->saveCaracteristicas($hotel_id, $edificio, $cars);

        $edificio->fill( $dataRequest );
        $edificio->save();



        return redirect()->to( url( "hotel/$hotel_id/edificios" ) )
        ->with('success', 'Actualizacion del hotel realizada correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($hotel_id, $id)
    {
        $edificio = Edificio::find($id);
        $edificio->delete();

        return response([
            'error' => false,
            'message' => "Edificio eliminado correctamente",
            'edificio' => $edificio
        ], 200)->header('Content-Type', 'application/json');
    }


    public function servicios( $hotel_id, $edificio_id ){
        return dd( $edificio_id );
    }
}
