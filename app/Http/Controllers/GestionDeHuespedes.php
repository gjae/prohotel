<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Hotel;
use App\Models\Habitacion;
use App\Models\Persona;
use App\Models\Factura;
use App\Models\FacturaServicio as FS;
use App\Models\FacturaPago as FP;


use DB;
use Auth;
use Carbon\Carbon;

class GestionDeHuespedes extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($hotel_id)
    {
        return dd($hotel_id);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($hotel_id , Request $request)
    {
        $habitacion = Habitacion::find( $request->h );

        if( $habitacion->ocupada == 1 )
            return redirect()->to( url( "hotel/$hotel_id/gestion/habitaciones" ) );

        return view('pages.gestion_hospedaje', [
            'hotel' => Hotel::find($hotel_id),
            'set_hotel' => false,
            'user' => Auth::user(),
            'persona' => !is_null( Auth::user()->persona ),
            'habitacion' => Habitacion::find($request->h),
            'factura' => false
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($hotel_id , Request $request)
    {   

        $carbon = Carbon::now()->diffInDays( Carbon::parse($request->ingreso_at) );

        if(  $carbon <= 0)
            return redirect()->to( url( "hotel/$hotel_id/gestion/hospedaje/create?h=$request->habitacion_id" ) )->with("error", "La fecha de ingreso es inferior a la fecha actual");

        DB::beginTransaction();
        try {

            // ACTUALIZACION DE LOS DATOS DE LA PERSONA
            $p = Persona::whereIdentificacion($request->identificacion)->first();
            if( is_null($p ) ){
                $p = new Persona;
                $persona = Persona::create( $request->only( $p->fillable ) );
            }
            $p->fill( $request->only( $p->fillable ) );
            $p->save();

            // CREAR FACTURA
            $dataFactura = $request->only( Factura::fillables() );
            $dataFactura['ingreso_at'] = Carbon::parse( $dataFactura['ingreso_at'] )->format('Y-m-d H:i:s');
            $dataFactura['salida_at'] = Carbon::parse( $dataFactura['salida_at'] )->format('Y-m-d H:i:s');

            $factura = new Factura( $dataFactura );
            $factura->persona_id = $p->id;
            $factura->save();

            // SERVICIOS A FACTURAR

            if( $request->has("hotel_servicio_id") && $request->hotel_servicio_id != null ){
                foreach ($request->hotel_servicio_id as $key => $value) {
                    $fs = new FS( $request->all() );
                    $fs->servicio_id = $value;
                    $fs->factura_id =  $factura->id;
                    $fs->fecha_registro = Carbon::now()->format( 'Y-m-d H:i:s');
                    $fs->save();
                }
            }
            

            // PAGOS DE FACTURA

            $fp = new FP( $request->all() );
            $fp->factura_id = $factura->id;
            $fp->save();

            // ACTUALIZACION DEL ESTADO DE LA HABITACION

            $h = Habitacion::find( $request->habitacion_id );
            $h->ocupada = 1;
            $h->save();
            DB::commit();

            return redirect()->to( url( "hotel/$hotel_id/gestion/habitaciones" ) )->with('success', 'Factura realizada correctamente');
        } catch (\Exception $e) {
            
            DB::rollback();

            return dd($e->getMessage());

        }
        return dd( $request->all() );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($hotel_id, $id, Request $request)
    {
        $factura = Factura::find($id);
        $habitaciones = $factura->habitacion;

        return view('pages.gestion_hospedaje', [
            'hotel' => Hotel::find($hotel_id),
            'set_hotel' => false,
            'user' => Auth::user(),
            'persona' => !is_null( Auth::user()->persona ),
            'habitacion' => Habitacion::find($request->h),
            'factura' => $factura
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function reserva($hotel_id, $factura_id){
        $factura = Factura::find($factura_id);
        $hotel = Hotel::find($hotel_id);
        $user = Auth::user();
        $persona = !is_null($user->persona);

        $respuesta = [
            'error' => false,
            'html' => \View::make('pages.partials.acciones_reserva', [
                'factura' => $factura,
                'hotel' => $hotel,
                'user' => $user,
                'persona' => $persona
            ])->render(),
            'modal_title' => 'Gestion de reserva'
        ];

        return response($respuesta, 200)->header('Content-Type', 'application/json');
    }
}
