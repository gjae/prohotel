<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'persona_id', 'rol', 'cadena', 'user_creador_id'
    ];

    public $date = ['deleted_at'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function hotel(){
        return $this->hasOne('App\Models\Hotel');
    }

    public function puesto(){
        return $this->hasOne('App\Models\HotelUser');
    }

    public function servicios(){
        return $this->hasMany('App\Models\Servicio');
    }

    public function tipos_habitaciones(){
        return $this->hasMany('App\Models\TipoHabitacion');
    }

    public function persona(){
        return $this->belongsTo('App\Models\Persona');
    }

    public function setPasswordAttribute($old){
        $this->attributes['password'] = bcrypt($old);
    }
}
