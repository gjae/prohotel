@extends('layouts.dashboard')

@section('content')
  <header class="main-header">
    <!-- Logo -->
    <a href="../../index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <!--<span class="logo-mini"><b>A</b>LT</span> -->
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>{{ $hotel->nombre }}</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
      </div>
    </nav>
  </header>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="http://theivykey.com/images/no-profile-image.png" class="img-circle" alt="User Image"> 
        </div>
        <div class="pull-left info">
          <p>{{ $persona ? $persona->nombres.' '.$persona->apellidos : $user->cadena }}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
     <!-- <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form> -->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">Navegación</li>

        @if( Auth::user()->rol == 'CDA_HOTEL' || Auth::user()->rol == 'ADMIN' )
	        <li class="treeview">
	          <a href="#">
	            <i class="fa fa-cog"></i>
	            <span>Configuración de hotel</span>
	            <span class="pull-right-container">
	              <span class="label label-primary pull-right">4</span>
	            </span>
	          </a>
	          <ul class="treeview-menu">
             <!-- <li><a href="{{ url("hotel/$hotel->id/config") }}"><i class="fa fa-circle-o"></i>Hotel</a></li> -->
              <li class="treeview">
                <a href="#"><i class="fa fa-circle-o"></i> Edificios
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="{{ url("hotel/$hotel->id/edificios") }}"><i class="fa fa-circle-o"></i>Edificios</a></li>
                  <li><a href="{{ url("hotel/$hotel->id/edificios/caracteristicas") }}"><i class="fa fa-circle-o"></i>Caracteristicas</a></li>
                </ul>
              </li>
	            
              <li class="treeview">
                <a href="#"><i class="fa fa-circle-o"></i> Habitaciones
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li>
                    <a href="{{ url("hotel/$hotel->id/habitaciones/tipos") }}">
                      <i class="fa fa-circle-o"></i>Tipos
                    </a>
                  </li>
                  <li>
                    <a href="{{ url("hotel/$hotel->id/servicios") }}">
                      <i class="fa fa-circle-o"></i>Servicios
                    </a>
                  </li>
                  <li>
                    <a href="{{ url("hotel/$hotel->id/habitaciones") }}">
                      <i class="fa fa-circle-o"></i>Distribución
                    </a>
                  </li>
                </ul>
              </li>
              <li><a href="{{ url("hotel/$hotel->id/eliminar") }}" class="text-danger"><i class="fa fa-circle-o"></i>Eliminar este hotel</a></li>
	            </li>
	          </ul>
	        </li>
	    @endif
        <li>
          <a href="{{ url( "hotel/$hotel->id/gestion/habitaciones" ) }}">
            <i class="fa fa-hotel"></i> <span>Habitaciones</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-green">new</small>
            </span>
          </a>
        </li>
        <li>
          <a href="{{ url( "hotel/$hotel->id/gestion/huespedes" ) }}">
            <i class="fa fa-users"></i> <span>Huespedes</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-green">new</small>
            </span>
          </a>
        </li>
        <li><a href="{{ url('logout') }}"><i class="fa fa-circle-o text-red"></i> <span>Cerrar sesión</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
</aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        @yield('title_for_wrapper')
      </h1>
     <!-- <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Layout</a></li>
        <li class="active">Collapsed Sidebar</li>
      </ol> -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">@yield('panel_header')</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          @section('dash_content')
          @show
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Footer
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>



@endsection