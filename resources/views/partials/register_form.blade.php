<div class="form-group has-feedback">
    <input type="email" name="email" value="{{ isset( $user ) && !is_bool($user) ? $user->email : '' }}" class="form-control" placeholder="Email">
    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
</div>

@if (  !isset($user) || is_bool( $user )  )
	<div class="form-group has-feedback">
	    <input type="password" name="password" min="6" class="form-control" placeholder="Contraseña">
	     <span class="glyphicon glyphicon-lock form-control-feedback"></span>
	</div>
	<div class="form-group has-feedback">
	    <input type="password" name="password_confirmation" class="form-control" placeholder="Repita la contraseña">
	    <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
	</div>
@endif
