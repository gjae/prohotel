<form action="{{ url($url) }}" method="POST">
	  @csrf
	@if ( !is_bool( $habitacion ) )
		@method('PUT')
	@endif

	<div class="form-row">
		<div class="col-sm-6 col-md-6 col-lg-6">
			<label for="">Tipo</label>
			<select name="tipo_habitacion_id" id="tipo_habitacion_id" class="form-control" required>
				<option value="">-- SELECCIONE UNO --</option>
				@foreach (App\Models\TipoHabitacion::whereUserId(Auth::user()->id )->get() as $tipo)
					<option 
						{{ is_bool( $habitacion ) ? '' : ( $habitacion->tipo_habitacion_id == $tipo->id ) ? 'selected': '' }}  
						value="{{ $tipo->id }}"
					>
						{{ $tipo->denominacion }}
					</option>
				@endforeach
			</select>
		</div>
		<div class="col-sm-6 col-md-6 col-md-6">
			<label for="">Numero de habitación</label>
			<input type="text" value="{{ is_bool($habitacion) ? '': $habitacion->codigo }}" placeholder="Codigo / Número de habitación" class="form-control" name="codigo" required="" id="codigo">
		</div>
	</div>

	<div class="form-row">
		<div class="col-sm-8 col-lg-8 col-md-8">
			<label for="">Edificio</label>
			<select name="edificio_id" id="edificio_id" class="form-control" required onChange="pisos(event, this)">
				<option value="">-- SELECCIONE UNO --</option>
				@foreach (App\Models\Edificio::whereHotelId( $hotel->id )->get() as $edificio)
					<option 
						{{ is_bool( $habitacion ) ? '' : ( $habitacion->edificio_id == $edificio->id ) ? 'selected': '' }}
						value="{{ $edificio->id }}-{{ $edificio->cantidad_pisos }}"
					>
						{{ $edificio->nombre }} / ( {{  $edificio->codigo }} )
					</option>
				@endforeach
			</select>
		</div>
		<div class="col-sm-4 col-md-4 col-lg-4">
			<label for="">Piso</label>
			<select name="piso_edificio" id="piso_edificio" class="form-control">
				@if ( !is_bool($habitacion) )

					<option value="{{ $habitacion->piso_edificio }}">
						{{ $habitacion->piso_edificio }}
					</option>
					<option value="PB">PB</option>
					@for ($i = 0; $i <  $habitacion->edificio->cantidad_pisos ; $i++)
						@if ( $habitacion->piso_edificio != ($i + 1 ) )
							<option value="{{ $i + 1 }}">
								{{ $i +1 }}
							</option>
							
						@endif
					@endfor
				@endif
			</select>
		</div>
		
	</div>
	<div class="form-row">
		<div class="col-sm-12 col-lg-12 col-md-12">
			<label for="">Servicios disponibles</label>
			<select name="servicios[]" id="" class="form-control select2" multiple="multiple" >
				@foreach (App\Models\Servicio::whereUserId( Auth::user()->id )->whereTipoServicio('HABITACION')->get() as $servicio)
					<option 
						value="{{ $servicio->id }}"
						{{ !is_bool($habitacion) && App\Models\HotelServicio::whereServicioId( $servicio->id )->whereHabitacionId($habitacion->id)->count() > 0 ? 'selected' : ''   }}
					>
						{{ $servicio->nombre }} ( {{ $servicio->codigo }} )
					</option>
				@endforeach
			</select>
		</div>
	</div>
	  <div class="form-row">
	  	<div class="col-sm-12 col-md-12 col-lg-12">
	  		<strong class="text-danger">
	  			Nota: no olvide hacer la descripción mas especifica posible
	  		</strong>
	  	</div>
	  </div>

	@include('partials.footer_modals')
</form>