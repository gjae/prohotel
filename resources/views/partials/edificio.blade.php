<form action="{{ url($url) }}" method="post">
	@csrf
	@if ( !is_bool( $edificio ) )
		@method('PUT')
	@endif
	<div class="form-group has-feedback form-row">
		<div class="col-sm-6 col-lg-6 col-md-6">
			<label for="">Nombre del edificio</label>
			<input type="text" value="{{ !is_bool( $edificio ) ? $edificio->nombre : '' }}" placeholder="Nombre del edificio" required class="form-control" name="nombre" id="nombre">
		</div>
		<div class="col-sm-3 col-md-3 col-lg-3">
			<label for="">Codigo</label>
			<input type="text" value="{{ !is_bool( $edificio ) ? $edificio->codigo : '' }}" required class="form-control" name="codigo" placeholder="Codigo del edificio" id="codigo">
		</div>
		<div class="col-sm-3 col-md-3 col-lg-3 has-feedback">
			<label for=""># De pisos</label>
			<input type="number" value="{{ !is_bool( $edificio ) ? $edificio->cantidad_pisos : '' }}" class="form-control" required="" placeholder="Numero de pisos" name="cantidad_pisos" id="cantidad_pisos">
		</div>
	</div>

	<div class="form-group has-feedback form-row">
		<div class="col-sm-12 col-lg-12 col-md-12">
			<label for="">Ubicación</label>
			<textarea name="ubicacion" id="ubicacion" cols="30" rows="1" class="form-control">{{ !is_bool( $edificio ) ? $edificio->ubicacion : '' }}</textarea>
		</div>
	</div>

	<div class="form-row">
		<div class="col-sm-12 col-lg-12 col-md-12">
			
		</div>
	</div>
	<div class="form-row">
		<div class="col-sm-12 col-md-12 col-lg-12">
			<h3 class="page_header">Caracteristicas</h3>
            <select class="form-control select2" multiple="multiple" data-placeholder="Select a State"
                        style="width: 100%;" name="caracteristicas[]">

                 @foreach (App\Models\Caracteristica::all() as $caracteristica)
                 	<option value="{{ $caracteristica->id }}" {{  !is_bool($edificio) && App\Models\CaracteristicaEdificio::whereEdificioId($edificio->id)->whereCaracteristicaId($caracteristica->id)->count() > 0 ? 'selected' : '' }} >
                 		{{ $caracteristica->denominacion }}
                 	</option>
                 @endforeach
              	<!--<option {{ !is_bool( $edificio ) && $edificio->tiene_wifi == 1 ? 'selected': '' }} value="tiene_wifi">
              		Wifi
              	</option>
                <option {{ !is_bool( $edificio ) && $edificio->tiene_zona_descanso == 1 ? 'selected': '' }} value="tiene_zona_descanso">
                	Zona de descanso
                </option>
                <option {{ !is_bool( $edificio ) && $edificio->tiene_ascensor == 1 ? 'selected': '' }} value="tiene_ascensor">
                	Tiene ascensor
                </option>
                <option {{ !is_bool( $edificio ) && $edificio->con_cabinas_internet == 1 ? 'selected': '' }} value="con_cabinas_internet">
                	Cabinas de internet
                </option>-->
            </select>
		</div>
		<!--<div class="col-sm-6 col-md-6 col-lg-6">
			<h3 class="page_header">Servicios</h3>
			<select name="servicios[]" multiple="multiple" name="servicios[]" data-placeholder="Seleccione los servicios disponibles" id="servicios" class="form-control select2">
				@foreach (App\Models\Servicio::whereUserId($hotel->user_id)->get() as $servicio)
					<option value="{{ $servicio->id }}">
						{{ $servicio->nombre }} ( {{ $servicio->codigo }} )
					</option>
				@endforeach
			</select>
		</div> -->
	</div>

	<!--<div class="form-row">
		<div class="col-sm-12 col-lg-12 col-md-12">
			<h3 class="page_header">Asociar/quitar servicios</h3>
		    <table class="table table-bordered table-striped datatables">
		        <thead>
		           <tr>
		           	<th>Codigo</th>
		              <th>Nombre</th>
		              <th>Descripcion</th>
		              <th>Costo</th>
		              <th align="center">
		              	<input type="checkbox" class="checkAll">
		              </th>
		            </tr>
		         
		        </thead>
		        <tbody>
					@foreach (App\Models\Servicio::whereUserId( $hotel->user_id )->whereTipoServicio('EDIFICIO')->get() as $servicio)
						<tr>
							<td>{{ $servicio->codigo }}</td>
							<td>
								{{ $servicio->nombre }}
							</td>
							<td>
								{{ $servicio->descripcion }}
							</td>
							<td>
								{{ number_format($servicio->costo, 2, ",", ".") }}
							</td>
							<td align="center">
								<label for="">
								<input type="checkbox" name="servicios[]" value="{{ $servicio->id }}" class="minimal check">
								</label>
							</td>	
						</tr>
					@endforeach
		        </tbody>
		    </table>
		</div>
	</div> -->
	<br><br>
	<hr>
	<br><br>
	@include('partials.footer_modals')
</form>