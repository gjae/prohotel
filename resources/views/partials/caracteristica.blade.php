<form action="{{ url($url) }}" method="POST">
	
	@csrf
	@if ( !is_bool($caracteristica) )
		@method('PUT')
	@endif
	
	<div class="form-row">
		<div class="col-sm-12 col-md-12 col-lg-12">
			<label for="">Caracteristica</label>
			<input type="text" value="{{ is_bool($caracteristica) ? '' : $caracteristica->denominacion }}" placeholder="Denominacion de la caracteristica" class="form-control" name="denominacion" required="" id="denominacion"> 
		</div>
	</div>

	<div class="form-row">
		<div class="col-sm-12 col-md-12 col-lg-12">
			<label for="">Descripción</label>
			<textarea name="descripcion" id="descripcion" cols="30" rows="2" class="form-control" >{{ is_bool($caracteristica) ? '' : $caracteristica->descripcion }}</textarea>
		</div>

	</div>

	<div class="form-row">
		@include('partials.footer_modals')
	</div>
</form>