<form action="{{ url($url) }}" method="post">
	@csrf
	@if ( !is_bool( $servicio ) )
		@method('PUT')
	@endif
	<div class="form-row has-feedback">
		<div class="col-sm-12 col-md-12 col-lg-12">
			<label for="">
				Nombre del servicio
				<input type="text" value="{{ !is_bool($servicio) ? $servicio->nombre : '' }}" class="form-control" name="nombre" required="" placeholder="Nombre del servicio" id="nombre">
			</label>
			<label for="">
				Codigo
				<input type="text" value="{{ !is_bool($servicio) ? $servicio->codigo : '' }}" class="form-control" id="codigo" name="codigo" required="" placeholder="Codigo del servicio">
			</label>
		</div>
	</div>
	<div class="form-row">
		<div class="col-sm-4 col-lg-4 col-md-4">
			<label for="">
				Costo del servicio
			</label>
			<input type="text" value="{{ !is_bool($servicio) ? $servicio->costo : '' }}" class="form-control" name="costo" required="" placeholder="Valor" id="costo">
		</div>
		<div class="col-sm-8 col-lg-8 col-md-8">
			<label for="">Tipo de servicio</label>
			<select name="tipo_servicio" id="tipo_servicio" class="form-control" required="">
				<option {{ !is_bool($servicio) && $servicio->tipo_servicio == "HABITACION" ? 'selected' : '' }} value="HABITACION">
					Servicio para las habitaciones
				</option>
			</select>
		</div>
	</div>
	<div class="form-row">
		<div class="col-sm-12 col-lg-12 col-md-12">
			<label for="">Descripción</label>
			<textarea name="descripcion" id="descripcion" cols="30" rows="1" class="form-control">{{ !is_bool($servicio) ? $servicio->descripcion : '' }}</textarea>
		</div>
	</div>

	<div class="form-row">
		<div class="col-sm-6 col-md-6 col-lg-6">
			<label for="">Hora de inicio del servicio</label>
			<input type="time" name="hora_inicio" value="{{ !is_bool($servicio) ? $servicio->hora_inicio->format('H:i:s') : '' }}" placeholder="Dejar en blanco para servicio permanente" class="form-control timepicker">
		</div>
		<div class="col-sm-6 col-md-6 col-lg-6">
			<label for="">Hora de finalización</label>
			<input type="time" value="{{ !is_bool($servicio) ? $servicio->hora_fin->format('H:i:s') : '' }}" class="form-control timepicker" name="hora_fin" id="hora_fin">
		</div>
	</div>
	<div class="form-row">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-lg-12 col-md-12">
					<strong class="text-danger">
						Ojo: La hora debe reflejarse en formato de 24 horas, ejemplo: 14:00 ( Equivale a: 02:00 PM)
					</strong>
				</div>
			</div>
		</div>
	</div>
	@include('partials.footer_modals')
</form>