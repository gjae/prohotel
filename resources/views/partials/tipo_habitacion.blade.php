<form action="{{ url($url) }}" method="POST">
	  @csrf
	@if ( !is_bool( $tipo_habitacion ) )
		@method('PUT')
	@endif
	  <div class="form-row">
	  	<div class="col-sm-12 col-lg-12 col-md-12">
		  	<label for="">
		  		Denominación del tipo
		  		
		  	</label>
		  	<input class="form-control" value="{{ is_bool( $tipo_habitacion ) ? '': $tipo_habitacion->denominacion }}" name="denominacion" required placeholder="Denominacion" id="denominacion">
	  	</div>
	  </div>

	  <div class="form-row">
	  	<div class="col-sm-4 col-md-4 col-lg-4">
	  		<label for="">Costo</label>
	  		<input type="text" value="{{ is_bool( $tipo_habitacion ) ? '': $tipo_habitacion->costo }}"  class="form-control" name="costo" required="" placeholder="Costo de este tipo" id="costo">
	  	</div>
	  	<div class="col-sm-4 col-md-4 col-lg-4">
	  		<label for="">Costo preferencial (1)</label>
	  		<input type="text" value="{{ is_bool( $tipo_habitacion ) ? '': $tipo_habitacion->costo_preferencial_1 }}"  class="form-control" name="costo_preferencial_1" required="" placeholder="Costo de este tipo" id="costo_preferencial_1">

	  	</div>
	  	<div class="col-sm-4 col-md-4 col-lg-4">
	  		<label for="">Costo preferencial (2)</label>
	  		<input type="text" value="{{ is_bool( $tipo_habitacion ) ? '': $tipo_habitacion->costo_preferencial_2 }}"  class="form-control" name="costo_preferencial_2" required="" placeholder="Costo de este tipo" id="costo_preferencial_2">
	  		
	  	</div>
	  </div>
	  <div class="form-row">
	
		<div class="col-md-12 col-lg-12 col-md-12">
			<label for="">Descripción del tipo</label>
			<textarea name="descripcion" id="" cols="30" rows="4" class="form-control" required="">{{ is_bool( $tipo_habitacion ) ? '': $tipo_habitacion->descripcion }}</textarea>
		</div>

	  </div>

	  <div class="form-row">
	  	<div class="col-sm-12 col-md-12 col-lg-12">
	  		<strong class="text-danger">
	  			Nota: no olvide hacer la descripción mas especifica posible
	  		</strong>
	  	</div>
	  </div>

	@include('partials.footer_modals')
</form>