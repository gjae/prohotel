@php
	$url = ( $persona ) ? "users/$user->id" : 'users';
@endphp
<form action="{{ url( $url ) }}" method="post">
	@csrf
	@if ( $persona )
		@method('PUT')
	@endif
	<input type="hidden" name="user_creador_id" value="{{ Auth::user()->id }}">
	<div class="form-row has-feedback">
		<div class="col-sm-6 col-lg-6 col-md-6">
			<label for="">
				Nombres
				<input type="text" value="{{ $persona ? $persona->nombres : '' }}" required placeholder="Ingrese nombre de la persona" name="nombres" class="form-control" id="nombres">
			</label>
		</div>
		<div class="col-sm-6 col-lg-6 col-md-6">
			<label for="">
				Apellidos
				<input type="text" value="{{ $persona ? $persona->apellidos : '' }}" required placeholder="Ingrese apellido de la persona" name="apellidos" class="form-control" id="apellidos">
			</label>
		</div>

		<div class="col-sm-3 col-lg-3 col-md-3">
			<div class="form-group has-feedback">
				<label for="">Tipo</label>
				<select name="tipo_identificacion" required id="" class="form-control">
					<option {{ $persona && $persona->tipo_identificacion == 'NIT' ? 'selected': '' }} value="NIT">NIT</option>	
					<option {{ $persona && $persona->tipo_identificacion == 'RUC' ? 'selected': '' }} value="RUC">RUC</option>
				</select>
			</div>
		</div>
		<div class="col-sm-9 col-md-9 col-lg-9">
			<div class="form-group">
				<label for="">Identificación</label>
				<input type="text"  value="{{ $persona ? $persona->identificacion : '' }}"  placeholder="Identificacion del usuario" required class="form-control" name="identificacion" id="identificacion">
			</div>
		</div>
	</div>
	<div class="form-row">
		<div class="col-sm-12 col-lg-12 col-md-12">
			<label for="">Telefono</label>
			<input type="phone" value="{{ $persona ? $persona->telefono : '' }}"  required placeholder="Telefono" class="form-control" name="telefono" id="telefono">
		</div>
		<div class="col-sm-12 col-md-12 col-lg-12">
			<label for="">Dirección</label>
			<textarea name="direccion" value="{{ $persona ? $persona->direccion : '' }}"  id="" cols="30" rows="1" class="form-control"></textarea>
		</div>
		<br><br>
		<h3 class="page_heaer">
			Datos de acceso del usuario
		</h3>
	</div>

	<div class="form-row">
		@include('partials.register_form')
	</div>
	<div class="form-row">
		<div class="col-sm-12 col-lg-12 col-md-12">
			<label for="">Tipo de usuario</label>
			<select name="rol" id="" class="form-control">
				<option  {{ $user && $user->rol == 'USER' ? 'selected' : '' }} value="USER">Usuario estandar</option>
				<option {{ $user && $user->rol == 'CONTADOR' ? 'selected' : '' }} value="CONTADOR">Contador</option>
				<option {{ $user && $user->rol == 'CAJA' ? 'selected' : '' }} value="CAJA">Cajero / recepcionista</option>
				<option {{ $user && $user->rol == 'ADMIN' ? 'selected' : '' }} value="ADMIN">Administrador</option>
			</select>
		</div>
	</div>
	<div class="form-row">
		<div class="col-sm-12 col-lg-12 col-md-12">
            <label>Hotel asignado</label>
           	<select class="form-control select2" name="hotel_id" style="width: 100%;">
           			<option value="QUITAR">Quitar / no asignar</option>
            	@foreach (App\Models\Hotel::whereUserId(Auth::user()->id)->get() as $hotel)
            	    <option  {{ ( $user->puesto !== null && $hotel->id == $user->puesto->hotel_id )? 'selected' : '' }} value="{{ $hotel->id }}">{{ $hotel->nombre }}</option>
            	@endforeach    
            </select>
		</div>
	</div>
	
	<hr>
	<br>
	<br>
	@include('partials.footer_modals')
</form>