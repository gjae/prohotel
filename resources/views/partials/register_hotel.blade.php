<form action="{{ url('hotel') }}" method="post">
	@csrf
	<input type="hidden" name="user_id" value="{{ Auth::user()->id }}">

	<div class="form-row">
		<div class="col-sm-12">
			<div class="form-group has-feedback">
				<input type="text" required class="form-control" name="nombre" placeholder="Nombre del hotel" id="nombre">
				<span class="fa fa-home form-control-feedback"></span>
			</div>
		</div>
	</div>

	<div class="form-row">
		<div class="col-sm-12">
			<div class="form-group">
				<label for="">Dirección</label>
				<textarea name="direccion" class="form-control" id="" cols="30" rows="1"></textarea>
			</div>
		</div>
	</div>
	
	<div class="form-row">
		<div class="col-sm-8">
			<label for="">¿Tiene estacionamiento?</label>
			<label for="">
				<input type="radio" class="minimal-red" name="estacionamiento" value="0">
				No
			</label>
			<label for="">
				<input type="radio" checked class="minimal-red" name="estacionamiento" value="1">
				Si
			</label>
		</div>
	</div>

	
	<div class="row">
		<div class="col-sm-12">
			@include('partials.footer_modals')
		</div>
	</div>
</form>