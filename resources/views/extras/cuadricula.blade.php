    <div class="container">
      <div class="row">
        @php
          $total_edificios = App\Models\Edificio::whereHotelId( $hotel->id )->count();
        @endphp
        @for ($i = 0; $i < $total_edificios; $i++)
          <div class="col-sm-{{ $i }} col-m-"></div>
        @endfor
      </div>
    </div>
    <section id="row_habitacions" class="row">
        @foreach ($habitaciones as $habitacion)
            <div class="col-lg-3 col-xs-6 card-room" data-groups='["animal"]'>
                      <!-- small box -->
                <div class="small-box {{ $habitacion->ocupada ? 'bg-red' : 'bg-aqua' }}">
                    <div class="inner">
                        <h3>{{ ucfirst($habitacion->codigo) }}  </h3>
          						 <h4>
          						 	Edificio {{ $habitacion->edificio->nombre }}
          						 	<strong>
          						 		Piso: {{ $habitacion->piso_edificio }}
          						 	</strong>
          						 </h4>
                    </div>
                    <p class="inner">
                      {{ $habitacion->tipo->denominacion }}
                    </p>
    		            <div class="icon">
    		              <i class="fa fa-hotel"></i>
    		            </div>
                    <a class="small-box-footer actions" type="hotel/{{ $hotel->id }}/gestion/habitaciones" action="{{ $habitacion->id }}">
                        Ver detalles <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
        @endforeach
    </section>
  
