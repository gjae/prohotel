<section id="table_rooms">
	<div class="col-sm-12 col-lg-12 col-md-12">
		<table class="table table-bordered table-striped datatables">
			<thead>
				<tr>
					<th>Numero</th>
					<th>Edificio</th>
					<th>Huesped</th>
					<th>Detalles</th>
					<th>Opciones</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($habitaciones as $habitacion)
				@php
					$total = 0;
					$color = "";
					if( !is_null($habitacion->reserva_activa) ){
						$total = $habitacion->reserva_activa->total - $habitacion->reserva_activa->pagos->sum('monto_pagado');
						$color = ( $total > 0 ) ? '#EDBC04' : '#FFFFFF';
					}
					if( !is_null( $habitacion->limpieza ) ){
						$color = "#EF2929";
					}
				@endphp
					<tr style="background-color: {{ $color }} ; color: {{ $color == "#EF2929"  ? "#FFFFFF" : "#000000" }}">
						<td>{{ $habitacion->codigo }}</td>
						<td>{{ $habitacion->edificio->nombre }}</td>
						<td>
							@if ( ! is_null($habitacion->reserva_activa) && $habitacion->reserva_activa->ingreso_at->diffInHours( \Carbon\Carbon::now() ) <= env('CANT_HORAS_ALERTA_RESERVA', 24) )
								{{ $habitacion->reserva_activa->persona->nombres.' '.$habitacion->reserva_activa->persona->apellidos }}
								@elseif(! is_null($habitacion->reserva_activa) && $habitacion->reserva_activa->ingreso_at->diffInHours( \Carbon\Carbon::now() ) >  env('CANT_HORAS_ALERTA_RESERVA', 24) )
								<strong>HABITACIÓN RESERVADA</strong>
							@endif
						</td>
						<td>{{ $habitacion->tipo->descripcion }}</td>
						<td>
							<a class="btn btn-small btn-success actions" type="hotel/{{ $hotel->id }}/gestion/habitaciones" action="{{ $habitacion->id }}">
								<i class="fa fa-eye"></i>
							</a>
							@if ( $habitacion->reserva_activa )
								<a  class="btn btn-small btn-primary actions" type="hotel/{{ $hotel->id }}/gestion/hospedaje/reserva" action="{{ $habitacion->reserva_activa->id }}">
									<i class="fa fa-book"></i>
								</a>
							@endif
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</section>