@extends('layouts.dashboard_hotels')
@section('title', 'Gestion de edificios')
@section('title_for_wrapper', 'Administracion de edificios')
@section('panel_header', 'Edificios')
@section('_css')

  <link rel="stylesheet" href="{{ asset('plugins/iCheck/all.css') }}">
@endsection

@section('dash_content')
    <a class="btn btn-app actions" type="hotel/{{ $hotel->id }}/edificios" action="create">
        <i class="fa fa-home"></i> Nuevo edificio
    </a>
    <table class="table table-bordered table-striped datatables">
        <thead>
           <tr>
              <th>Nombre</th>
              <th>Codigo</th>
              <th># de habitaciones</th>
              <th># de pisos</th>
              <th>Opciones</th>
            </tr>
         
        </thead>
        <tbody>
			@foreach (App\Models\Edificio::whereHotelId($hotel->id)->get() as $edificio)
				<tr>
					<td>{{ $edificio->nombre }}</td>
					<td>{{ $edificio->codigo }}</td>
					<td>{{ $edificio->total_habitaciones }}</td>
					<td>{{ $edificio->cantidad_pisos }}</td>
					<td>
						<button class="btn btn-primary btn-small actions" type="hotel/{{ $hotel->id }}/edificios/{{ $edificio->id }}" action="edit">
							<i class="fa fa-edit"></i>
						</button>
						<button class="btn btn-danger btn-small delete" type="hotel/{{ $hotel->id }}/edificios" data-delete="{{ $edificio->id }}">
							<i class="fa fa-remove"></i>
						</button>
            <a href="{{ url("hotel/$hotel->id/edificios/$edificio->id/servicios") }}" class="btn btn-success btn-small">
              <i class="fa fa-paperclip"></i>
            </a>
					</td>
				</tr>
			@endforeach
        </tbody>
    </table>
  <div class="modal modal fade" id="modal-primary">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
          <p>One fine body&hellip;</p>
        </div>
      </div>
            <!-- /.modal-content -->
    </div>
          <!-- /.modal-dialog -->
  </div>
@endsection
@section('jquery')

<script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script>
  $(document).ready( function(){
    $('.select2').select2()
    $('.actions').on('click', function(){
      var modal = $("#modal-primary");
      var url = location.protocol+'//'+location.host+`/${$(this).attr('type')}/${ $(this).attr('action') }`
      $.getJSON(url, {}, function(resp){
        if( resp.error ) modal = null;
        $(".modal-body").html( resp.html );
        $('.modal-title').text(resp.modal_title)
        modal.modal({open: true})
      });
    });

    $('.delete').on('click', function(){

      if( confirm('¿seguro que desea ejecutar esta acción? No podra ser revertida.') )
      {
        var url = location.protocol+'//'+location.host+`/${ $(this).attr('type')}/${ $(this).attr('data-delete') }`;
        $.post(url, { _method : 'DELETE', _token: "{{ csrf_token() }}" }, function(resp){
          if( confirm( resp.message ) ) {
          	location.reload()
          }
        });
      }
    });
    $('.datatables').DataTable();
  });
</script>

@endsection