@extends('layouts.dashboard_hotels')
@section('title', 'Servicios')
@section('title_for_wrapper', 'Manejo de servicios')
@section('panel_header', 'Servicios creados')
@section('_css')

  <link rel="stylesheet" href="{{ asset('plugins/iCheck/all.css') }}">
@endsection

@section('dash_content')
    <a class="btn btn-app actions" type="hotel/{{ $hotel->id }}/servicios" action="create">
        <i class="fa fa-paperclip"></i> Nuevo Servicios
    </a>
    <table id="example1" class="table table-bordered table-striped">
        <thead>
           <tr>
           	<th>Codigo</th>
              <th>Nombre</th>
              <th>Descripcion</th>
              <th>Tipo de servicio</th>
              <th>Costo</th>
              <th>Opciones</th>
            </tr>
         
        </thead>
        <tbody>
			@foreach (App\Models\Servicio::whereUserId( $user->id )->get() as $servicio)
				<tr>
					<td>{{ $servicio->codigo }}</td>
					<td>
						{{ $servicio->nombre }}
					</td>
					<td>
						{{ $servicio->descripcion }}
					</td>
					<td>
						{{ $servicio->tipo_servicio }}
					</td>
					<td>
						{{ number_format($servicio->costo, 2, ",", ".") }}
					</td>
					<td>
						<button class="btn btn-primary btn-small actions" type="hotel/{{ $hotel->id }}/servicios/{{ $servicio->id }}" action="edit">
							<i class="fa fa-edit"></i>
						</button>
						<button class="btn btn-danger btn-small delete" type="hotel/{{ $hotel->id }}/servicios" data-delete="{{ $servicio->id }}">
							<i class="fa fa-remove"></i>
						</button>
					</td>		
				</tr>
			@endforeach
        </tbody>
    </table>
  <div class="modal modal fade" id="modal-primary">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
          <p>One fine body&hellip;</p>
        </div>
      </div>
            <!-- /.modal-content -->
    </div>
          <!-- /.modal-dialog -->
  </div>
@endsection
@section('jquery')

<script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script> 
<script src="{{ asset('plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>

<script>
  $(document).ready( function(){
    $('.select2').select2()
    $('.actions').on('click', function(){
      var modal = $("#modal-primary");
      var url = location.protocol+'//'+location.host+`/${$(this).attr('type')}/${ $(this).attr('action') }`
      $.getJSON(url, {}, function(resp){
        if( resp.error ) modal = null;
        $(".modal-body").html( resp.html );
        $('.modal-title').text(resp.modal_title)
        modal.modal({open: true})
      });
    });

    $('.delete').on('click', function(){

      if( confirm('¿seguro que desea ejecutar esta acción? No podra ser revertida.') )
      {
        var url = location.protocol+'//'+location.host+`/${ $(this).attr('type')}/${ $(this).attr('data-delete') }`;
        $.post(url, { _method : 'DELETE', _token: "{{ csrf_token() }}" }, function(resp){
          if( confirm( resp.message ) ) {
          	location.reload()
          }
        });
      }
    });
    $('#example1').DataTable();


    $('.timepicker').timepicker({
        showInputs: false
      })
  });
</script>

@endsection