@extends('layouts.dashboard_hotels')
@section('title', 'Tipos de habitaciones')
@section('title_for_wrapper', 'Habitaciones - configuracion de tipos')
@section('panel_header', '')
@section('_css')
 <link rel="stylesheet" href="{{ asset('plugins/iCheck/all.css') }}">
@endsection

@section('dash_content')
    <a class="btn btn-app actions" type="hotel/{{ $hotel->id }}/habitaciones/tipos" action="create">
        <i class="fa fa-home"></i> Nuevo tipo
    </a>
    <table class="table table-bordered table-striped datatables">
        <thead>
           <tr>
              <th>Nombre</th>
              <th>Descripcion</th>
              <th>Costo</th>
              <th>Opciones</th>
            </tr>
         
        </thead>
        <tbody>
			@foreach (App\Models\TipoHabitacion::whereUserId($user->id)->get() as $tipo)
				<tr>
					<td>{{ $tipo->denominacion }}</td>
					<td>{{ $tipo->descripcion }}</td>
					<td>{{ $tipo->costo }}</td>
					<td>
						<button class="btn btn-primary btn-small actions" type="hotel/{{ $hotel->id }}/habitaciones/tipos/{{ $tipo->id }}" action="edit">
							<i class="fa fa-edit"></i>
						</button>
						<button class="btn btn-danger btn-small delete" type="hotel/{{ $hotel->id }}/habitaciones/tipos" data-delete="{{ $tipo->id }}">
							<i class="fa fa-remove"></i>
						</button>
            <a href="{{ url("hotel/$hotel->id/tipos/$tipo->id/servicios") }}" class="btn btn-success btn-small">
              <i class="fa fa-paperclip"></i>
            </a>
					</td>
				</tr>
			@endforeach
        </tbody>
    </table>
  <div class="modal modal fade" id="modal-primary">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
          <p>One fine body&hellip;</p>
        </div>
      </div>
            <!-- /.modal-content -->
    </div>
          <!-- /.modal-dialog -->
  </div>
@endsection
@section('jquery')

<script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script>
  $(document).ready( function(){
    $('.select2').select2()
    $('.actions').on('click', function(){
      var modal = $("#modal-primary");
      var url = location.protocol+'//'+location.host+`/${$(this).attr('type')}/${ $(this).attr('action') }`
      $.getJSON(url, {}, function(resp){
        if( resp.error ) modal = null;
        $(".modal-body").html( resp.html );
        $('.modal-title').text(resp.modal_title)
        modal.modal({open: true})
      });
    });

    $('.delete').on('click', function(){

      if( confirm('¿seguro que desea ejecutar esta acción? No podra ser revertida.') )
      {
        var url = location.protocol+'//'+location.host+`/${ $(this).attr('type')}/${ $(this).attr('data-delete') }`;
        $.post(url, { _method : 'DELETE', _token: "{{ csrf_token() }}" }, function(resp){
          if( confirm( resp.message ) ) {
          	location.reload()
          }
        });
      }
    });
    $('.datatables').DataTable();
  });
</script>

@endsection