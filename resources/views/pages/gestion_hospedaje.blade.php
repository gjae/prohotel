@extends('layouts.dashboard_hotels')
@section('title', 'Nuevo huesped')
@section('title_for_wrapper', 'Gestion de hospedaje')
@section('panel_header', 'Registro de reserva')
@section('_css')

  <link rel="stylesheet" href="{{ asset('plugins/iCheck/all.css') }}">
 <style>
  .my-group .form-control{
      width:50%;
  }
  .reserva-data{
    text-align: center;
  }
 
</style>
@endsection

@section('dash_content')

  @if( \Session::has("error") )
  <div class="row">
    <div class="col-sm-12 col-md-12">
      <div class="alert alert-danger">
        {{ \Session::get("error") }}
      </div>
    </div>
  </div>
  @endif
  <input type="hidden" id="_ivg_porcentaje" value="{{ env('IVG_PORCENTAJE', 16) }}">
	<section id="form-hospedake">
		<form action='{{ url("hotel/$hotel->id/gestion/hospedaje") }}' method="POST">
			<div class="form-row">
        @csrf
        
        <input type="hidden" name="hotel_id" id="hotel_id" value="{{ $hotel->id }}">
				<div class="col-sm-4 col-lg-4 col-md-4">
					<label for="">Identificación</label>
			        <div class="input-group my-group"> 
			            <select id="tipo_identificacion" name="tipo_identificacion" class="selectpicker form-control">
			                <option value="NIT">NIT</option>
			                <option value="RUC">RUC</option>
			            </select> 
			            <input type="text" class="form-control" name="identificacion" placeholder="Identificación" required onkeypress="buscarPersona(event, this)" />
			        </div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4">
					<label for="">Nombre</label>
					<input type="text" class="form-control data-persona" id="nombres" required="" name="nombres" readonly="">
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4">	
					<label for="">Apellidos</label>
					<input type="text" class="form-control data-persona" id="apellidos" required="" name="apellidos" readonly="">
				</div>
			</div>
			<div class="form-row">
				<div class="col-sm-4 col-md-4 col-lg-4">
					<label for="">Telefono</label>
					<input type="text" class="form-control data-persona" id="telefono" name="telefono" required="" placeholder="Telefono de contacto" readonly>
				</div>
				<div class="col-sm-4 col-md-4 col-lg-4">
					<label for="">Correo electronico</label>
					<input type="text" class="form-control data-persona" readonly="" id="correo_electronico" name="correo_electronico" required="">
				</div>
				<div class="col-sm-4 col-md-4 col-lg-4">
					<label for="">Origen del huesped</label>
					<input type="text" class="form-control data-persona" name="origen" required="" readonly="">
				</div>
			</div>
      <div class="form-row">
        <div class="col-sm-12 col-lg-12 col-md-12">
          <h3 class="page-header">
            Datos de la reserva
          </h3>
        </div>
      </div>

      <div class="form-row">
        <div class="col-sm-12 col-md-12 col-lg-12">
          <label for="">Razón de la visita</label>
          <input type="text" placeholder="Razón de su visita" required class="form-control reserva-data" name="motivo_visita" id="motivo_visita">
        </div>
        <div class="col-sm-4 col-lg-4 col-lg-4">
          <label for="">Tipo de habitación</label>
          <input type="text" value="{{ $habitacion->tipo->denominacion }}" class="form-control reserva-data" name="tipo_habitacion" readonly="" id="tipo_habitacion">
        </div>
        <div class="col-sm-4 col-lg-4 col-lg-4">
          <label for="">Edificio</label>
          <input type="text" readonly value="{{ $habitacion->edificio->nombre }}" class="form-control reserva-data" name="edificio" id="edificio">
        </div>
        <div class="col-sm-4 col-lg-4 col-lg-4">
          <label for="">Habitación</label>
          <input type="hidden" name="habitacion_id" value="{{ $habitacion->id }}">
          <input type="text" value="{{ $habitacion->codigo }}" class="form-control reserva-data" name="habitacion" readonly="" id="habitacion">
        </div>
      </div>
      <div class="form-row">
        <div class="col-sm-12 co-lg-12 col-md-12">
          <h3 class="page-header">Datos de facturación</h3>
        </div>
        <div class="col-sm-4 col-lg-4 col-md-4">
          
          <label for="">Factura a nombre de...</label>
          <div class="input-group my-group"> 
            <select id="tipo_identificacion" required name="tipo_identificacion_factura" class="selectpicker form-control" required>
                <option value="" disabled selected>Tipo</option>
                <option value="NIT">NIT</option>
                <option value="RUC">RUC</option>
            </select> 
            <input type="text" class="form-control" tipo="factura" name="identificacion_factura" placeholder="Test" onkeypress="buscarPersona(event, this)" />
          </div>
        </div>
        <div class="col-sm-3 col-lg-3 col-md-3">
          <label for="">Titular de la factura</label>
          <input type="text" required class="form-control" name="titular_factura" id="titular_factura">
        </div>
        <div class="col-sm-1 col-lg-1 col-md-1">
            <label for="">Tipo</label>
            <select name="facturado_por" id="facturado_por" class="form-control" required="">
              <option value="" disabled selecte>Modalidad</option>
              <option value="HRS">Horas</option>
              <option value="D">Dìa</option>
              <option value="M">Mes</option>
              <option value="A">Año</option>
            </select>
        </div>
        <div class="col-sm-4 col-lg-4 col-md-4">
          <label for="">Ingreso / Salida</label>
          <div class="input-group">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
            </div>
            <input type="hidden" name="ingreso_at" id="ingreso_at">
            <input type="hidden" name="salida_at" id="salida_at">
            <input type="text" class="form-control pull-right" id="reservationtime">
          </div>

        </div>
      </div>

      <div class="form-row">
        <div class="col-sm-2 col-md-2 col-lg-2">
          <label for="">Tiempo total en  <b id="tipo_factura">Horas</b> </label>
          <input type="text" class="form-control reserva-data" name="total_tiempo" value="0" readonly="" id="total_tiempo">
        </div>
        <div class="col-sm-2 col-md-2 col-lg-2">
          <label for="">Por hora</label>
          <input type="text" value="{{ $habitacion->tipo->costo }}" class="form-control" readonly="" id="precio_hora">
        </div>
        <div class="col-sm-2 col-md-2 col-lg-2">
          <label for="">Por día</label>
          <input type="text" value="{{ $habitacion->tipo->costo_preferencial_1 }}" class="form-control" readonly="" id="precio_dia">
        </div>
        <div class="col-sm-2 col-md-2 col-lg-2">
          <label for="">Por mes</label>
          <input type="text" value="{{ $habitacion->tipo->costo_preferencial_2 }}" class="form-control" readonly="" id="precio_mes">
        </div>
        <div class="col-sm-2 col-lg-2 col-md-2">
          <label for="">Comprobante</label>
          <input type="text" readonly="" placeholder="Comprobante DEBITO / CREDITO" class="form-control" name="nro_referencia" id="nro_comprobante">
        </div>
        <div class="col-sm-2 col-md-2 col-lg-2">
          <label for="">Modo de pago</label>
          <select name="tipo_pago" id="tipo_pago" class="form-control" required="">
            <option value="">Elija uno</option>
            <option value="EFECTIVO">Efectivo</option>
            <option value="CHEQUE">Cheque</option>
            <option value="DEBITO">Debito</option>
            <option value="CREDITO">Credito</option>
          </select>
        </div>
      </div>
      
      <div class="form-row">
        <div class="col-sm-12 col-lg-12 col-md-12">
          <h4 class="page-header">Servicios adicionales</h4>
        </div>
      </div>
      <div class="form-row">
        
        <div class="col-sm-12 col-lg-12 col-md-12">
          <table class="table table-bordered table-striped datatables">
            <thead>
              <tr>
                <th>Servicio</th>
                <th>Descripcion</th>
                <th>Costo</th>
                <th>Seleccionar</th>
              </tr>
            </thead>
            <tbody>
              @foreach ( App\Models\HotelServicio::whereHotelId($hotel->id)->where('habitacion_id', '<>', 0)->get() as $key => $servicio)
                <tr>
                  <td>
                    {{ $servicio->servicio->nombre }}
                  </td>
                  <td>
                    {{ $servicio->servicio->descripcion }}
                  </td>
                  <td>
                    {{ $servicio->servicio->costo }}
                  </td>
                  <td>
                    <input type="checkbox" id="checked{{ $key }}" class="add_servicio" valor_servicio="{{ $servicio->servicio->costo }}" name="hotel_servicio_id[]" value="{{ $servicio->servicio_id }}">
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>

      </div>

      <div class="form-row">
        <div class="container">
          
          <div class="row">
            <div class="col-sm-4 col-lg-4 col-md-4">
              <div class="row">
              </div>
            </div>
            <div class="col-sm-8 col-lg-8 col-md-8">
              
                <!-- TOTALES -->
                <div class="form-row">
                            
                  <div class="col-sm-3 col-lg-3 col-md-3 col-sm-offset-9">
                    <label for="">Total pagado</label>
                    <input type="text" value="0" required class="form-control" name="monto_pagado" id="monto_pagado">
                  </div>
                </div>
                <div class="form-row">
                  <div class="col-sm-3 col-lg-3 col-md-3 col-sm-offset-9">
                    <label for="">Total base</label>
                    <input type="text" name="subtotal" id="subtotal" readonly class="form-control">
                  </div>
                </div>
              </div>
              <div class="form-row">
                <div class="col-sm-3 col-lg-3 col-md-3 col-sm-offset-9">
                  <label for="">Total IVG</label>
                  <div class="input-group">
                      <span class="input-group-addon">
                        <input type="checkbox" checked name="tiene_ivg" id="tiene_ivg">
                      </span>
                      <input type="text" value="0" class="form-control" name="ivg" id="ivg" readonly>
                  </div>
                </div>
              </div>
              <div class="form-row">
                <div class="col-sm-3 col-lg-3 col-md-3 col-sm-offset-9">
                  <label for="">Total general</label>
                  <input type="text" name="total" readonly="" value="0" class="form-control" id="total_general">
                </div>
              </div>

              <div class="form-row">
                <div class="col-sm-12 col-lg-12 col-md-12">
                  <div class="btn-group">
                    <a class="btn btn-danger">Cancelar</a>
                    <button class="btn btn-success">Generar reserva</button>
                  </div>
                </div>
              </div>
              <!-- fin de los totales --> 
            </div>

          </div>
      </div>

		</form>
	</section>
  <div class="modal modal fade" id="modal-primary">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
          <p>One fine body&hellip;</p>
        </div>
      </div>
            <!-- /.modal-content -->
    </div>
          <!-- /.modal-dialog -->
  </div>
@endsection
@section('jquery')

<script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script src="{{ asset('plugins/shuffle/shuffle.min.js') }}"></script>
<script src="{{ asset('bower_components/moment/min/moment.min.js') }}"></script>
<script>
  $(document).ready( function(){


    function apply(start, end, label){
      $("#ingreso_at").val( start.format('YYYY-MM-DD h:mm A') );
      $("#salida_at").val( end.format('YYYY-MM-DD h:mm A') );

      var tipo =  $("#facturado_por").val();
      tipo = tipo == 'D' ? 'days' : tipo == 'M' ? 'months' : tipo == 'A'  ? 'years' : tipo == 'HRS' ? 'hours' : 'hours';

      let date = end.diff(start, tipo); 
     //alert( $("#_ivg_porcentaje").val()  );
      $("#total_tiempo").val( date );
      actualizar_totales();

    }

    $('#reservationtime').daterangepicker({ 
      timePicker: true, 
      timePickerIncrement: 30, 
      format: 'MM/DD/YYYY h:mm A', 
      locale: { 
        format: 'DD-MM-YYYY h:mm A'
       } 
    }, apply);

    $("#tiene_ivg").on('change', function(){
      $("#ivg").val(0);
      actualizar_totales();
    });

    function actualizar_totales( calc_total = true, servicios = 0 ){
      var tipo_pago = $("#facturado_por").val();
      var costo = tipo_pago == 'D' ? 'dia' : tipo_pago == 'M' ? 'mes' : tipo_pago == 'HRS' ? 'hora' : 'dia';
      costo = $("#precio_"+costo).val();
      var pago = $("#monto_pagado").val();


      var tiempo = $("#total_tiempo").val();
      var subtotal = ( costo * tiempo );

      var servicios = 0;
      var checks = $("[name='hotel_servicio_id[]']" );
      
      for( var i = 0; i < checks.length; i ++ ){
         servicios += ( $( "#checked"+i ).is(":checked") ) ? parseFloat($("#checked"+i ).attr('valor_servicio')) : 0;
      }
      subtotal += parseFloat(servicios);

      $("#subtotal").val( subtotal );



      var ivg = ( $("#tiene_ivg").is(":checked") ) ? ( subtotal * $("#_ivg_porcentaje").val() )/ 100 : 0 ;
      $("#ivg").val( ivg );

      if(calc_total)
        $("#total_general").val( (ivg + subtotal ) - pago );

    }

    $("#monto_pagado").on('keyup', function(){
      actualizar_totales(false);
    });

    $(".add_servicio").on('change', function(){
/*       var checks = $("[name='hotel_servicio_id[]']" );
      
      let costo = 0;
      for( var i = 0; i < checks.length; i ++ ){
         costo += ( $( "#checked"+i ).is(":checked") ) ? parseFloat($("#checked"+i ).attr('valor_servicio')) : 0;
      }*/
       actualizar_totales(true,  0);
    });

    $("#tipo_pago").on('change', function(){

      switch( $(this).val() ){
        case 'DEBITO':
        case 'CREDITO':
        case 'CHEQUE':
          $("#nro_comprobante").removeAttr('readonly');
        break;

        case 'EFECTIVO':
          $("#nro_comprobante").val("");
          $("#nro_comprobante").attr('readonly', true);
        break;
      }

    });

    function facturado_en(tipo){
      return tipo == 'D' ? 'Dias' : tipo == 'M' ? 'Meses' : tipo == 'A' ? 'Años' : tipo == 'HRS' ? 'Horas' : '';
    }

    $("#facturado_por").on('change', function(){
      var tipo = $(this).val();
      $("#tipo_factura").text( facturado_en(tipo)  );

      apply( moment( $("#ingreso_at").val() ), moment( $("#salida_at").val() ), '' )
      actualizar_totales();
    })
    $.fn.dataTable.ext.search.push(
        function( settings, data, dataIndex ) {
            var filter = $(".active-filter").attr("by");
            var edificio = data[1]; // use data for the age column
      
            if ( ( filter == undefined || filter == edificio ) || ( filter == "todos" ) )
            {
                return true;
            }


            return false;
        }
    );

    var table = $('.datatables').DataTable();
    $(".filter").on('click', function(){
      $(".active-filter").removeClass("active-filter");
      $(this).addClass("active-filter");
      table.draw()
    });

    // SHUFFLE JS
   /* const shuffleInstance = new Shuffle(document.getElementById('row_habitacions'), {
      itemSelector: '.card-room',
      sizer: '.js-shuffle-sizer'
    });*/

    $("#animal").on('click', function(){
      shuffleInstance.filter('animal');
    });

    // END SHUFFLE JS

    $('.select2').select2()
    $('.actions').on('click', function(){
      var modal = $("#modal-primary");
      var url = location.protocol+'//'+location.host+`/${$(this).attr('type')}/${ $(this).attr('action') }`
      $.getJSON(url, {}, function(resp){
        if( resp.error ) modal = null;
        $(".modal-body").html( resp.html );
        $('.modal-title').text(resp.modal_title)
        modal.modal({open: true})
      });
    });

    $('.delete').on('click', function(){

      if( confirm('¿seguro que desea ejecutar esta acción? No podra ser revertida.') )
      {
        var url = location.protocol+'//'+location.host+`/${ $(this).attr('type')}/${ $(this).attr('data-delete') }`;
        $.post(url, { _method : 'DELETE', _token: "{{ csrf_token() }}" }, function(resp){
          if( confirm( resp.message ) ) {
          	location.reload()
          }
        });
      }
    });
    $('.datatables').DataTable();
  });

  function buscarPersona(e, input){
  	if(e.keyCode  == 13){
      var url = location.protocol+'//'+location.host+'/hotel/'+$("#hotel_id").val()+'/personas/'+input.value+'?tipo='+$("#tipo_identificacion").val();

      $.getJSON(url, {}, function(resp){
        if( !resp.error){
            $(".data-persona").removeAttr('readOnly');
            Object.keys( resp.persona ).forEach( key => {
                $("#"+key).val( resp.persona[key] );
            });
        }
      });
  	}
  }
</script>

@endsection