@extends('layouts.dashboard_hotels')
@section('title', 'Habitaciones del habitacion')
@section('title_for_wrapper', 'Administracion de habitaciones')
@section('panel_header', 'Habitaciones')
@section('_css')

  <link rel="stylesheet" href="{{ asset('plugins/iCheck/all.css') }}">
@endsection

@section('dash_content')

  <section id="filters" class="center-block">

    <div class="row">
      <div class="col-sm-9 col-lg-9 col-md-9">
        @foreach (App\Models\Edificio::whereHotelId($hotel->id)->get() as $edificio)
            
          <button class="btn btn-app filter" by="{{ $edificio->nombre }}" id="{{ $edificio->nombre }}">
            <i class="fa fa-home"></i>
            {{ $edificio->nombre }}
          </button>
        @endforeach        
          <button class="btn btn-app filter" by="todos" id="todos">
            <i class="fa fa-home"></i>
            Todos
          </button>
        </div>
        <div class="col-sm-3 col-md-3 col-lg-3">
          <a class="btn btn-app actions" type="hotel/{{ $hotel->id }}/gestion/habitaciones" action="reportes">
            <i class="fa fa-print"></i>
            reportes
          </a>
        </div>

      </div>
  </section>
  
  @include('extras.tabla')
  <div class="modal modal fade" id="modal-primary">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
          <p>One fine body&hellip;</p>
        </div>
      </div>
            <!-- /.modal-content -->
    </div>
          <!-- /.modal-dialog -->
  </div>
@endsection
@section('jquery')

<script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script src="{{ asset('plugins/shuffle/shuffle.min.js') }}"></script>
<script>
  $(document).ready( function(){
    $.fn.dataTable.ext.search.push(
        function( settings, data, dataIndex ) {
            var filter = $(".active-filter").attr("by");
            var edificio = data[1]; // use data for the age column
      
            if ( ( filter == undefined || filter == edificio ) || ( filter == "todos" ) )
            {
                return true;
            }


            return false;
        }
    );

    var table = $('.datatables').DataTable();
    $(".filter").on('click', function(){
      $(".active-filter").removeClass("active-filter");
      $(this).addClass("active-filter");
      table.draw()
    });

    // SHUFFLE JS
   /* const shuffleInstance = new Shuffle(document.getElementById('row_habitacions'), {
      itemSelector: '.card-room',
      sizer: '.js-shuffle-sizer'
    });*/

    $("#animal").on('click', function(){
      shuffleInstance.filter('animal');
    });

    // END SHUFFLE JS

    $('.select2').select2()
    $('.actions').on('click', function(){
      var modal = $("#modal-primary");
      var url = location.protocol+'//'+location.host+`/${$(this).attr('type')}/${ $(this).attr('action') }`
      $.getJSON(url, {}, function(resp){
        if( resp.error ) modal = null;
        $(".modal-body").html( resp.html );
        $('.modal-title').text(resp.modal_title)
        modal.modal({open: true})
      });
    });

    $('.delete').on('click', function(){

      if( confirm('¿seguro que desea ejecutar esta acción? No podra ser revertida.') )
      {
        var url = location.protocol+'//'+location.host+`/${ $(this).attr('type')}/${ $(this).attr('data-delete') }`;
        $.post(url, { _method : 'DELETE', _token: "{{ csrf_token() }}" }, function(resp){
          if( confirm( resp.message ) ) {
          	location.reload()
          }
        });
      }
    });
    $('.datatables').DataTable();
  });



  function txtInsidenciaChange(e, check){
    let txtInput = document.getElementById('descripcion_insidencia');
    if( check.checked )
      txtInput.readOnly = false;
    else{
      txtInput.value = '';
      txtInput.readOnly = true;
    }
  }
</script>

@endsection