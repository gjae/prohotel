@extends('layouts.dashboard_hotels')
@section('title', 'Gestion de pagosn')
@section('title_for_wrapper', 'Administracion de pagos')
@section('panel_header', 'Pagos del huesped')
@section('_css')

  <link rel="stylesheet" href="{{ asset('plugins/iCheck/all.css') }}">
@endsection

@section('dash_content')

  <section id="filters" class="center-block">
	<section id="table_rooms">

		@if ( $factura->total - $factura->pagos->sum('monto_pagado') > 0 )
			<a class="btn btn-app actions" type="hotel/{{ $hotel->id }}/gestion/pagos/create?factura={{ $factura_id }}">
				<i class="fa fa-gratipay" ></i>
				Registrar pago
			</a>
		@endif
		<div class="col-sm-12 col-lg-12 col-md-12">
			<table class="table table-bordered table-striped datatables">
				<thead>
					<tr>
						<th>ID #</th>
						<th>Registrado el día</th>
						<th>Referencia</th>
						<th>Tipo de pago</th>
						<th>Monto</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($factura->pagos as $pago)
						<tr>
							<td>{{ $pago->id }}</td>
							<td>{{ $pago->created_at->format('d/m/Y h:i A') }}</td>
							<td>{{ $pago->nro_referencia }}</td>
							<td>{{ $pago->tipo_pago }}</td>
							<td>{{ $pago->monto_pagado }}</td>
						</tr>
					@endforeach
				</tbody>
				<tfoot>
					<tr align="right">
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<th style="text-align: right;">Total pagado</th>
						<th  style="text-align: right;"> 
							{{ $factura->pagos->sum('monto_pagado') }}
						</th>
					</tr>
					<tr align="right">
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<th style="text-align: right;">Total por pagar</th>
						<th  style="text-align: right;"> 
							{{ $factura->total - $factura->pagos->sum('monto_pagado') }}
						</th>
					</tr>
				</tfoot>
			</table>

		</div>
	</section>
   
  <div class="modal modal fade" id="modal-primary">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
          <p>One fine body&hellip;</p>
        </div>
      </div>
            <!-- /.modal-content -->
    </div>
          <!-- /.modal-dialog -->
  </div>
@endsection
@section('jquery')

<script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script src="{{ asset('plugins/shuffle/shuffle.min.js') }}"></script>
<script>
  $(document).ready( function(){
    $.fn.dataTable.ext.search.push(
        function( settings, data, dataIndex ) {
            var filter = $(".active-filter").attr("by");
            var edificio = data[1]; // use data for the age column
      
            if ( ( filter == undefined || filter == edificio ) || ( filter == "todos" ) )
            {
                return true;
            }


            return false;
        }
    );

    var table = $('.datatables').DataTable();
    $(".filter").on('click', function(){
      $(".active-filter").removeClass("active-filter");
      $(this).addClass("active-filter");
      table.draw()
    });

    // SHUFFLE JS
   /* const shuffleInstance = new Shuffle(document.getElementById('row_habitacions'), {
      itemSelector: '.card-room',
      sizer: '.js-shuffle-sizer'
    });*/

    $("#animal").on('click', function(){
      shuffleInstance.filter('animal');
    });

    // END SHUFFLE JS

    $('.select2').select2()
    $('.actions').on('click', function(){
      var modal = $("#modal-primary");
      var url = location.protocol+'//'+location.host+`/${$(this).attr('type')}/${ $(this).attr('action') }`
      $.getJSON(url, {}, function(resp){
        if( resp.error ) modal = null;
        $(".modal-body").html( resp.html );
        $('.modal-title').text(resp.modal_title)
        modal.modal({open: true})
      });
    });

    $('.delete').on('click', function(){

      if( confirm('¿seguro que desea ejecutar esta acción? No podra ser revertida.') )
      {
        var url = location.protocol+'//'+location.host+`/${ $(this).attr('type')}/${ $(this).attr('data-delete') }`;
        $.post(url, { _method : 'DELETE', _token: "{{ csrf_token() }}" }, function(resp){
          if( confirm( resp.message ) ) {
          	location.reload()
          }
        });
      }
    });
    $('.datatables').DataTable();
  });


	function format(e, input){
		//alert(input.value);
	}
</script>

@endsection