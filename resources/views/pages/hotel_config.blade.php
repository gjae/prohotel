@extends('layouts.dashboard_hotels')
@section('title', 'Configuración de hotel')
@section('title_for_wrapper', 'Configuración de hotel')
@section('panel_header', 'Hotel')
@section('_css')

  <link rel="stylesheet" href="{{ asset('plugins/iCheck/all.css') }}">
@endsection

@section('dash_content')
 
 <div class="container">
 	
 	<form action="{{ url("hotel/$hotel->id/config") }}" method="POST">
 		<input type="hidden" name="_method" value="PUT">
 		@csrf
 		<div class="row">
 			<div class="col-sm-6 col-md-6 col-lg-6">
 				<label for="">Cadena / Propietario</label>
 				<input type="text" readonly value="{{ $hotel->cadena->cadena }}" class="form-control">
 			</div>
 			<div class="col-sm-6 col-md-6 col-lg-6">
 				<label for="">Nombre del hotel</label>
 				<input type="text" value="{{ $hotel->nombre }}" class="form-control" name="nombre" required="" id="nombre">
 			</div>
 		</div>
 		<div class="row">
 			<div class="col"></div>
 		</div>
 	</form>

 </div>

  <div class="modal modal fade" id="modal-primary">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
          <p>One fine body&hellip;</p>
        </div>
      </div>
            <!-- /.modal-content -->
    </div>
          <!-- /.modal-dialog -->
  </div>
@endsection
@section('jquery')

<script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script>
  $(document).ready( function(){
    $('.select2').select2()
    $('.actions').on('click', function(){
      var modal = $("#modal-primary");
      var url = location.protocol+'//'+location.host+`/${$(this).attr('type')}/${ $(this).attr('action') }`
      $.getJSON(url, {}, function(resp){
        if( resp.error ) modal = null;
        $(".modal-body").html( resp.html );
        $('.modal-title').text(resp.modal_title)
        modal.modal({open: true})
      });
    });

    $('.delete').on('click', function(){

      if( confirm('¿seguro que desea ejecutar esta acción? No podra ser revertida.') )
      {
        var url = location.protocol+'//'+location.host+`/${ $(this).attr('type')}/${ $(this).attr('data-delete') }`;
        $.post(url, { _method : 'DELETE', _token: "{{ csrf_token() }}" }, function(resp){
          if( confirm( resp.message ) ) {
          	location.reload()
          }
        });
      }
    });
    $('.datatables').DataTable();
  });
</script>

@endsection