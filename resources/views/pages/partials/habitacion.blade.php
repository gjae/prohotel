<form action="">
	<div class="row">
		<div class="col-sm-3 col-md-3 col-lg-3">
			<label for="">Número / Codigo</label>
			<input type="text" value="{{ $habitacion->codigo }}" class="form-control" readonly="">
		</div>
		<div class="col-sm-6 col-lg-6 col-md-6">
			<label for="">Edificio</label>
			<input type="text" value="{{ $habitacion->edificio->nombre }}" class="form-control" readonly="">
		</div>
		<div class="col-sm-3 col-md-3 col-lg-3">
			<label for="">Piso</label>
			<input type="text" value="{{ $habitacion->piso_edificio }}" class="form-control" readonly="">
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12 col-lg-12 col-md-12">
			<h3 class="page-header">Costos</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-4 col-lg-4 col-md-4">
			<label for="">Costo</label>
			<input style="text-align: center;" type="text" value="{{ $habitacion->tipo->costo }}" class="form-control" readonly="">
		</div>
		<div class="col-sm-4 col-lg-4 col-md-4">
			<label for="">Preferencial</label>
			<input style="text-align: center;" type="text" value="{{ $habitacion->tipo->costo_preferencial_1 }}" class="form-control" readonly="">
		</div>
		<div class="col-sm-4 col-lg-4 col-md-4">
			<label for="">Preferencial especial</label>
			<input style="text-align: center;" type="text" value="{{ $habitacion->tipo->costo_preferencial_2 }}" class="form-control" readonly="">
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12 col-md-12 col-lg-12">
			<label for="">Detalles</label>
			<textarea name="" id="" cols="30" rows="2" class="form-control" readonly="">{{ $habitacion->tipo->descripcion }}</textarea>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12 col-md-12 col-lg-12">
			<div class="modal-footer">
			    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cerrar</button>

			    @if( $habitacion->ocupada == 0 )
				    <a href="{{ url("hotel/$hotel->id/gestion/hospedaje/create") }}?h={{ $habitacion->id }}" class="btn btn-success">
				    	Reserva
				    </a>
				    @elseif( !is_null( $factura ) )
				    <a href="{{ url("hotel/$hotel->id/gestion/hospedaje/$factura->id") }}?h={{ $habitacion->id }}" class="btn btn-success">
				    	Ver reserva
				    </a>

			    @endif
			    @if ( is_null( $habitacion->limpieza ) )
				    <a class="btn btn-danger actions_modals" type="hotel/{{ $hotel->id }}/gestion/habitaciones/{{ $habitacion->id }}/solicitar_limpieza">
				    	Solicitar limpieza
				    </a>
				    @else
				    <a class="btn btn-danger limpiada" type="hotel/{{ $hotel->id }}/gestion/habitaciones/{{ $habitacion->id }}/limpiada">
				    	Habitación limpiada
				    </a>
			    @endif
			</div>
		</div>
	</div>
	<script>
		$('.actions_modals').on('click', function(){
	      var modal = $("#modal-primary");
	      var url = location.protocol+'//'+location.host+`/${$(this).attr('type')}`
	      $.getJSON(url, {}, function(resp){
	        if( resp.error ) modal = null;
	        $(".modal-body").html( resp.html );
	        $('.modal-title').text(resp.modal_title)
	      });
	    });

	    $(".limpiada").on('click', function(){
	      var url = location.protocol+'//'+location.host+`/${$(this).attr('type')}`;

	      if( confirm("¿Seguro que desea realizar esta acción?") ){
		      $.post(url, {_token: "{{ csrf_token() }}"}, function(resp){
		      	alert(resp.message)
		        if( !resp.error )
		        	location.reload();
		      });
	      }
	    });
	</script>
</form>