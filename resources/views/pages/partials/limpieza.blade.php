<form action="{{ url("hotel/$hotel->id/gestion/habitaciones/$habitacion->id/solicitar_limpieza") }}" method="POST">
	@csrf
	
	<input type="hidden" name="habitacion_id" value="{{ $habitacion->id }}">
	<input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
	<div class="row">
		<div class="col-sm-12 col-md-12 col-lg-12">
			<label for="">¿Alguna insidencia?</label>
			<div class="input-group">
				<span class="input-group-addon">
					<input type="checkbox" name="insidencia" onclick="txtInsidenciaChange(event, this)" value="1">
				</span>
				<input type="text" class="form-control"  name="descripcion_insidencia" readonly="" id="descripcion_insidencia" placeholder="Descripcion de la insidencia">
			</div>
		</div>
		<div class="col-sm-6 col-sm-6 col-md-6">
			<label for="">¿Necesita cambio de sabanas?</label>
			<select name="cambio_sabanas" id="cambio_sabanas" class="form-control" required="">
				<option value="">-- ELIJA UNA OPCION --</option>
				<option value="1">SI, necesita sabanas limpias</option>
				<option value="0">NO</option>
			</select>
		</div>
		<div class="col-sm-6 col-md-6 col-lg-6">
			<label for="">Fecha de registro</label>
			<input type="text" readonly class="form-control" name="solicitud_at" value="{{ date('Y-m-d H:i:s') }}">
		</div>
	</div>


	<div class="row">
		@include('partials.footer_modals')
	</div>
</form>