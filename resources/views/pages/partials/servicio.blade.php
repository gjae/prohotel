<form action="{{ url("hotel/$hotel/gestion/servicios") }}" method="POST">
	@csrf
	<div class="row">
		<input type="hidden" id="tiene_ivg" value="{{ $factura->tiene_ivg }}">
		<input type="hidden" id="porcentaje_ivg" value="{{ env("IVG_PORCENTAJE", 0) }}">
		<input type="hidden" name="factura_id" value="{{ $factura->id }}">
		
		<input type="hidden" class="total_ivg" name="total_ivg" value="0">
		<input type="hidden" class="total" name="total" value="0">

		<input type="hidden" id="por_pagar" value="{{ $factura->total - $factura->pagos->sum('monto_pagado') }}">
		<div class="col-sm-12 col-md-12 col-lg-12">
			
			<table class="table table-bordered table-striped datatables-modal">
				<thead>
					<tr>
						<th>Codigo</th>
						<th>Servicio</th>
						<th>Costo</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					@foreach (App\Models\Servicio::whereUserId( ( $persona ? $user->user_creador_id : $user->id ) )->get() as $servicio)
						<tr>
							<td>
								{{ $servicio->codigo }}
							</td>
							<td>
								{{ $servicio->nombre }}
							</td>
							<td>
								{{ $servicio->costo }}
							</td>
							<td>
								<input type="checkbox" name="servicios[]" costo="{{ $servicio->costo }}" id="servicio-{{ $servicio->id }}" value="{{ $servicio->id }}" onchange="facturar()" class="fecturar_servicio">
							</td>
						</tr>
					@endforeach
				</tbody>
				<tfoot style="text-align: right;">
					<tr>
						<tt>&nbsp;</th>
						<th>&nbsp;</th>
						<th style="text-align: right;">IVG</th>
						<td class="total_ivg" style="text-align: right;"></td>
					</tr>
					<tr>
						<tt>&nbsp;</th>
						<th>&nbsp;</th>
						<th style="text-align: right;">Total adicional por servicios</th>
						<td class="total" style="text-align: right;"></td>
					</tr>
					<tr>
						<tt>&nbsp;</th>
						<th>&nbsp;</th>
						<th style="text-align: right;">Deuda actual</th>
						<td class="por_pagar" style="text-align: right;"> {{ $factura->total - $factura->pagos->sum('monto_pagado') }} </td>
					</tr>
					<tr>
						<tt>&nbsp;</th>
						<th>&nbsp;</th>
						<th style="text-align: right;">Deuda actualizada</th>
						<td class="nueva_deuda" style="text-align: right;"> {{ $factura->total - $factura->pagos->sum('monto_pagado') }} </td>
					</tr>
				</tfoot>
			</table>
			<div class="row">
				@include('partials.footer_modals')
			</div>


		<script>
			var table = $('.datatables-modal').DataTable();
			var tiene_ivg = $("#tiene_ivg").val() == "on" || $("#tiene_ivg").val() == 1 ? true : false;
			var porcentaje_ivg = $("#porcentaje_ivg").val();
			function facturar(){
				var servicios = $("[name='servicios[]']");

				let costo = 0;
				for(var i = 0; i< servicios.length ; i ++){
					let servicio = servicios[i];
					costo += ( servicio.checked ) ? parseFloat( servicio.getAttribute('costo') ) : 0;
				}

				let ivg = ( tiene_ivg ) ? ( costo * parseFloat( porcentaje_ivg ) ) / 100 : 0;
				let por_pagar = $("#por_pagar").val()
				$(".total_ivg").val( ivg );
				$(".total_ivg").html( ivg );
				$(".total").html( costo + ivg );
				$(".total").val( costo + ivg );

				$(".nueva_deuda").html( ( costo + ivg ) + parseFloat( por_pagar ) );
			};
		</script>
		</div>
	</div>
</form>