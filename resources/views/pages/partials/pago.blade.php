<form action="{{ url("hotel/$hotel->id/gestion/pagos") }}" method="POST">
	@csrf
	<input type="hidden" name="factura_id" value="{{ $factura->id }}">
	<div class="row">
		<div class="col-sm-6 col-md-6 col-lg-6">
			<label for="">Cliente</label>
			<input type="text" class="form-control" readonly="" value="{{ $factura->persona->nombres.' '.$factura->persona->apellidos }}">
		</div>
		<div class="col-sm-3 col-lg-3 col-md-3">
			<label for="">Total pagado</label>
			<input type="text" class="form-control" readonly="" value="{{ $factura->pagos->sum('monto_pagado') }}">
		</div>
		<div class="col-sm-3 col-lg-3 col-md-3">
			<label for="">Total por pagar</label>
			<input type="text" class="form-control" readonly="" value="{{ $factura->total - $factura->pagos->sum('monto_pagado') }}">
		</div>
	</div>
	<div class="row">
		<div class="col-sm-4 col-lg-4 col-md-4">
			<label for="">Medio de pago</label>
			<select name="tipo_pago" id="tipo_pago" class="form-control" required="">
				<option value="">-- ELIJA UNO --</option>
				<option value="DEBITO">Debito</option>
				<option value="CREDITO">Credito</option>
				<option value="EFECTIVO">Efectivo</option>
				<option value="CHEQUE">Cheque</option>
			</select>
		</div>
		<div class="col-sm-4 col-lg-4 col-md-4">
				<label for="">Comprobante</label>
				<input type="text" class="form-control" name="nro_referencia" id="nro_referencia">
		</div>
		<div class="col-sm-4 col-lg-4 col-md-4">
			<label for="">Monto pagado</label>
			<input type="text" style="text-align: right;" class="form-control" name="monto_pagado" required="" id="monto_pagado" onkeyup="format(event, this)">
		</div>
	</div>
	<div class="row">
		@include('partials.footer_modals')
	</div>
</form>