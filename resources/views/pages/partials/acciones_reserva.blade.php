<div class="row">
	<div class="col-sm-12 col-sm-12 col-md-12">
		<a class="btn btn-app actions" href="{{ url("hotel/$hotel->id /gestion/pagos/$factura->id") }}">
			<i class="fa fa-gratipay" ></i>
			Gestion de pagos
		</a>
		<a class="btn btn-app" href="{{ url("hotel/$hotel->id/gestion/servicios/$factura->id") }}">
			<i class="fa fa-bed"></i>
			Servicios
		</a>
		<a  class="btn btn-app" token="{{ csrf_token() }}" onclick="cerrarFactura(event, this)" factura="{{ $factura->id }}" hotel-id="{{ $hotel->id }}" >
			<i class="fa fa-archive"></i>
			Cerrar factura
		</a>
	</div>
</div>
<div class="row">
	<div class="col-sm-12 col-md-12 col-lg-12">
		@include('partials.footer_modals')
	</div>
</div>
<script>
	function cerrarFactura(e, btn){
		var url = location.protocol+'//'+location.host+`/hotel/${ btn.getAttribute("hotel-id") }/factura/${ btn.getAttribute("factura") }/cerrar`;

		if( confirm("¿Seguro que desea realizar esta acción?, al estar la factura cerrada se considera la habitación disponible en espera de limpieza y no se podra agregar mas montos a la factura") ){

				$.post(url, {_token: btn.getAttribute('token') }, function(resp){
					alert(resp.message);
					if( !resp.error )
						location.reload()

				});
			
			}
	}
</script>