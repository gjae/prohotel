<form method="POST" onsubmit="tiposRportes(event, form)">
	

	<input type="hidden" id="hotel_id" value="{{ $hotel->id }}">
	<div class="row">
		<div class="col-sm-6 col-md-6 col-lg-6 col-sm-offset-3" >
			<label for="">Tipo de reporte</label>
			<select name="tipo" id="tipo" class="form-control"  required="">
				<option value="POR_LIMPIEZA">Habitaciones por limpieza</option>
				<option value="MOROSOS">Huespedes morosos</option>

				<option value="NO_MOROSOS">Huespedes sin deuda</option>
				<option value="HABITACIONES_LIMPIAS">Habitaciones limpias</option>
				<option value="SERVICIOS_SIN_CONSUMIR">Servicios facturados sin consumir</option>
			</select>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6 col-md-6 col-lg-6 col-sm-offset-3" >
			<label for="">Edificio</label>
			<div class="">
				<select name="edificio_id" id="edificio_id" class="form-control" >
					<option value="TODO">Todos los edificios</option>
					@foreach ($hotel->edificios as $edificio)
						<option value="{{ $edificio->id }}">
							{{ $edificio->nombre }}
						</option>
					@endforeach
				</select>
			</div>
		</div>
	</div>
</form>

<div class="modal-footer">
	<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cerrar</button>
	<a onclick="tiposRportes(event, this)" class="btn btn-success">Generar</a>
</div>
<script>
function tiposRportes(e, formulario){
    e.preventDefault();
    var url = location.protocol+'//'+location.host+`/hotel/${ document.getElementById("hotel_id").value }/reportes?tipo=${ document.getElementById("tipo").value }&edificio=${ document.getElementById("edificio_id").value }`

    window.open(url, "width=260,height=500")
}


</script>