@extends('layouts.dashboard_hotels')
@section('title', 'Gestion de servicios')
@section('title_for_wrapper', 'Administracion de servicios por reserva')
@section('panel_header', 'Servicios solicitados')
@section('_css')

  <link rel="stylesheet" href="{{ asset('plugins/iCheck/all.css') }}">
@endsection

@section('dash_content')

  <section id="filters" class="center-block">
	<section id="table_rooms">

			<a class="btn btn-app actions" type="hotel/{{ $hotel->id }}/gestion/servicios/create?factura={{ $factura->id }}">
				<i class="fa fa-gratipay" ></i>
				Registrar servicio
			</a>

		<div class="col-sm-12 col-lg-12 col-md-12">
			<table class="table table-bordered table-striped datatables">
        <thead>
          <tr>
            <th>Codigo</th>
            <th>Servicio</th>
            <th>Descripcion</th>
            <th>Estado</th>
            <th>Opciones</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($factura->servicios as $servicio)
            <tr>
              <td>{{ $servicio->servicio->codigo }}</td>
              <td>{{ $servicio->servicio->nombre }}</td>
              <td>{{ $servicio->servicio->descripcion }}</td>
              <td>{{ $servicio->estado == 'NOCONS' ? 'PENDIENTE DE USO' : 'USADO' }}</td>
              <td>
                @if ( $servicio->estado == 'NOCONS' )
                  <a  class="btn btn-small btn-success delete" type="hotel/{{ $hotel->id }}/gestion/servicios" data-delete="{{ $servicio->id }}?type=CONSUMIDO">
                    <i class="fa  fa-check-square-o"></i>
                  </a>
                @endif
              </td>
            </tr>
          @endforeach
        </tbody>
			</table>

		</div>
	</section>
   
  <div class="modal modal fade" id="modal-primary">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
          <p>One fine body&hellip;</p>
        </div>
      </div>
            <!-- /.modal-content -->
    </div>
          <!-- /.modal-dialog -->
  </div>
@endsection
@section('jquery')

<script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script src="{{ asset('plugins/shuffle/shuffle.min.js') }}"></script>
<script>
  $(document).ready( function(){
    $.fn.dataTable.ext.search.push(
        function( settings, data, dataIndex ) {
            var filter = $(".active-filter").attr("by");
            var edificio = data[1]; // use data for the age column
      
            if ( ( filter == undefined || filter == edificio ) || ( filter == "todos" ) )
            {
                return true;
            }


            return false;
        }
    );

    var table = $('.datatables').DataTable();
    $(".filter").on('click', function(){
      $(".active-filter").removeClass("active-filter");
      $(this).addClass("active-filter");
      table.draw()
    });

    // SHUFFLE JS
   /* const shuffleInstance = new Shuffle(document.getElementById('row_habitacions'), {
      itemSelector: '.card-room',
      sizer: '.js-shuffle-sizer'
    });*/

    $("#animal").on('click', function(){
      shuffleInstance.filter('animal');
    });

    // END SHUFFLE JS

    $('.select2').select2()
    $('.actions').on('click', function(){
      var modal = $("#modal-primary");
      var url = location.protocol+'//'+location.host+`/${$(this).attr('type')}`
      $.getJSON(url, {}, function(resp){
        if( resp.error ) modal = null;
        $(".modal-body").html( resp.html );
        $('.modal-title').text(resp.modal_title)
        modal.modal({open: true})
      });
    });

    $('.delete').on('click', function(){

      if( confirm('¿seguro que desea ejecutar esta acción? No podra ser revertida.') )
      {
        var url = location.protocol+'//'+location.host+`/${ $(this).attr('type')}/${ $(this).attr('data-delete') }`;
        $.post(url, { _method : 'DELETE', _token: "{{ csrf_token() }}" }, function(resp){
          if( confirm( resp.message ) ) {
          	location.reload()
          }
        });
      }
    });
    $('.datatables').DataTable();
  });


	function format(e, input){
		//alert(input.value);
	}
</script>

@endsection