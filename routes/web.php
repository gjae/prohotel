<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->to( url('login') );
});



Route::group(['middleware' => 'auth'], function(){

	Route::group(['prefix' => 'hotel'], function(){

		Route::post("/{hotel_id}/factura/{factura_id}/cerrar", "GestionDeHabitacionesController@cerrarFactura");

		Route::post('/{hotel_id}/destroy', 'HotelController@destroy');
		Route::get('/{hotel_id}/config' , 'HotelController@config');
		Route::get('/{hotel_id}/eliminar' , function($hotel_id){
			$user = \Auth::user();
			return view('pages.verify_user', [
				'redirect_to' => null,
				'hotel_id' => $hotel_id,
				'action' => "hotel/$hotel_id/destroy",
				'method' => 'DELETE',
				'user' => $user
			]);
		});
		Route::put('/{hotel_id}/config' , 'HotelController@update');
		Route::resource('/{hotel_id}/edificios/caracteristicas', 'CaracteristicaController');
		Route::get("/{hotel_id}/reportes", "ReportesController@reportes");
		

		// -- RUTAS PARA LA PARTE ADMINISTRATIVA DE LAS HABITACIONES
		Route::resource('/','HotelController');
		Route::get('/{id}', 'HotelController@show')->name('show_hotel');
		Route::resource('/{hotel_id}/edificios', 'EdificioController');
		Route::resource('/{hotel_id}/servicios', 'ServiciosController');
		Route::resource('/{hotel_id}/edificios/caracteristicas', 'CaracteristicaController');
		Route::resource('/{hotel_id}/personas', 'PersonaController');

		Route::get('/{hotel_id}/edificios/{edificio_id}/servicios', 'EdificioController@servicios');


		Route::resource('/{hotel_id}/habitaciones/tipos', 'TiposHabitacionController');

		Route::resource('/{hotel_id}/habitaciones', 'HabitacionController');

		// -- RUTAS PARA LA PARTE DE LOS USUARIOS NO ADMINISTRADORES (USUARIOS GENERALES DE RECEPCION)

		Route::group(["prefix" => "/{hotel_id}/gestion"], function(){

			Route::get('habitaciones/reportes', 'GestionDeHabitacionesController@tiposDeReporte');
			Route::resource('habitaciones', 'GestionDeHabitacionesController');

			Route::get('habitaciones/{habitacion_id}/solicitar_limpieza', 'GestionDeHabitacionesController@solicitar_limpieza');


			Route::post('habitaciones/{habitacion_id}/solicitar_limpieza', 'GestionDeHabitacionesController@guardar_solicitud');
			Route::post('habitaciones/{habitacion_id}/limpiada', 'GestionDeHabitacionesController@limpiada');

			Route::resource("/hospedaje", 'GestionDeHuespedes');
			Route::resource("/pagos", 'GestionDePagos');
			Route::resource("/servicios", 'GestionDeServicios');
			Route::get('/hospedaje/reserva/{factura_id}', 'GestionDeHuespedes@reserva');
		});
	});

	Route::resource('users', 'UsersController');

	Route::get('manager', function(){
		return view('index');
	});

	Route::get('logout', function(){ \Auth::logout(); });

});

Route::get('/logout', function(){
	Auth::logout();
	return redirect()->to( url("login") );
});


Route::get('manager2', function(){
	return view('index');
});

Route::get('manager3', function(){
	return view('auth.login_2');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
